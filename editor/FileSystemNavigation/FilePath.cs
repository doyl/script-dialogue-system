﻿namespace Doyl.Utility.FileSystemNavigation
{
    public class FilePath
    {
        public static string GetDirectoryOfFile(string pathToFile)
        {
            string output = "";
            int index = pathToFile.LastIndexOf('/');
            if(index>0)
                output = pathToFile.Remove(index);
            return output;
        }

        public static string GetFileNameWithoutExtensionFromPath(string pathToFile)
        {
            return RemoveExtensionFromFileName(GetFileNameFromPath(pathToFile));
        }
        
        public static string GetFileNameFromPath(string path)
        {
            int index = path.LastIndexOf('/');
            string output = path.Substring(index + 1);
            return output;
        }

        public static string RemoveExtensionFromFileName(string fileName)
        {
            string output = fileName;
            int indexOfExtension = fileName.LastIndexOf('.');
            if(indexOfExtension>0)
                output = fileName.Remove(indexOfExtension);
            return output;
        }
        
        public static string GetInsideUnityPath(string path)
        {
            int indexOfAssets = path.IndexOf("Assets/");
            return path.Substring(indexOfAssets);
        }
    }
}
