﻿#if UNITY_EDITOR
using UnityEditor;

namespace Doyl.Utility.FileSystemNavigation
{
    [System.Serializable]
    public class DirectoryExplorer
    {
        public string CurrentPath { get; private set; }
        public string DefaultPath { get; private set; }
        public string Title { get; private set; }
        public string DefaultName { get; private set; }
    
        public string PathInUnity => FilePath.GetInsideUnityPath(CurrentPath);

        public DirectoryExplorer(string defaultPath, string title, string defaultName)
        {
            DefaultPath = defaultPath;
            CurrentPath = defaultPath;
            Title = title;
            DefaultName = defaultName;
        }
    
        public void ChooseDirectory()
        {
            string newFilePath =
                EditorUtility.OpenFolderPanel(Title, FilePath.GetDirectoryOfFile(CurrentPath), DefaultName);
            if (string.IsNullOrEmpty(newFilePath))
            {
                return;
            }
            CurrentPath = newFilePath;
        }
    }
}
#endif
