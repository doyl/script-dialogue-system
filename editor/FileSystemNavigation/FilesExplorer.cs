﻿#if UNITY_EDITOR
using UnityEditor;

namespace Doyl.Utility.FileSystemNavigation
{
    [System.Serializable]
    public class FilesExplorer
    {
        public string CurrentPath { get; private set; }
        public string DefaultPath { get; private set; }
    
        public string AcceptedExtension { get; private set; }
        public string Title { get; private set; }
    
        public string FileName => FilePath.GetFileNameFromPath(CurrentPath);

        public string FileNameWithoutExtension => FilePath.GetFileNameWithoutExtensionFromPath(CurrentPath);
    
        public string PathInUnity => FilePath.GetInsideUnityPath(CurrentPath);

        public FilesExplorer(string defaultPath, string acceptedExtension, string title)
        {
            DefaultPath = defaultPath;
            CurrentPath = defaultPath;
            AcceptedExtension = acceptedExtension;
            Title = title;
        }

        public void ChooseFile()
        {
            string newFilePath =
                EditorUtility.OpenFilePanel(Title, FilePath.GetDirectoryOfFile(CurrentPath), AcceptedExtension);
            if (string.IsNullOrEmpty(newFilePath))
            {
                return;
            }

            CurrentPath = newFilePath;
        }
    }
}
#endif
