﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes;
using UnityEditor;
using XNodeEditor;

namespace Doyl.ScriptDialogueSystem.Editor.ScriptScript
{
    [CustomNodeEditor(typeof(ConditionNode))]
    public class ConditionNodeEditor : NodeEditor
    {
        private ConditionNode conditionNode;

        public override void OnBodyGUI()
        {
            conditionNode = (ConditionNode)target;
            DialogueGraph graph = (DialogueGraph)conditionNode.graph;
            serializedObject.Update();
        
            base.OnBodyGUI();
            conditionNode.selectedIndex = EditorGUILayout.Popup("Condition", conditionNode.selectedIndex, graph.conditions.Select(t => t.conditionName).ToArray());
            if (conditionNode.selectedIndex < graph.conditions.Count)
            {
                conditionNode.conditionName = graph.conditions?[conditionNode.selectedIndex].conditionName;
                ArgumentsUi(graph.conditions?[conditionNode.selectedIndex].argumentDefinitions);   
            }
        }
    
        private void ArgumentsUi(List<NodeEditorArgumentDefinition> arguments)
        {
            if (conditionNode.argumentsValues.Count != arguments.Count)
            {
                conditionNode.argumentsValues =
                    new List<string>(new string[arguments.Count]);
            }
            for (int i=0; i<arguments.Count; ++i)
            {
                var arg = arguments[i];
                if (arg.variableType == ArgumentType.Int)
                {
                    conditionNode.argumentsValues[i] = EditorGUILayout.IntField(arg.name, int.Parse(conditionNode.argumentsValues[i])).ToString();
                }
                if (arg.variableType == ArgumentType.String)
                {
                    conditionNode.argumentsValues[i] = EditorGUILayout.TextField(arg.name, conditionNode.argumentsValues[i]);
                }
            }
        }
    }
}

#endif
