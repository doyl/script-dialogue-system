﻿#if UNITY_EDITOR
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Converter;
using Doyl.Utility.FileSystemNavigation;
using UnityEditor;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Editor.ScriptScript
{
    public class ScriptScriptConverterWindow : EditorWindow
    {
        [SerializeField] private FilesExplorer fileToConvert = new FilesExplorer("Assets/", "txt", "Choose file to convert.");
        [SerializeField] private DirectoryExplorer outputDirectory = new DirectoryExplorer("Assets/", "Choose output directory", "Dialogues");
        [SerializeField] private DialogueGraph dialogueGraph;
        private TextFileLoader textFileLoader = new TextFileLoader();
        private static DialogueLocalizationOperation dialogueLocalizationOperation;
        private static bool isInit;
        
        private bool convertFromGraph;
        //private SerializedProperty dialogueGraphProperty;
        
        [MenuItem("Window/Script Dialogues/Converter %_2")]
        static void Init()
        {
            dialogueLocalizationOperation = ScriptScriptDependencyProvider.GetSingleton().DialogueLocalizationOperation;
            var window = (ScriptScriptConverterWindow)EditorWindow.GetWindow(typeof(ScriptScriptConverterWindow), false, "Script Dialogues Converter");
            //window.maxSize = new Vector2(700, 700);]
            isInit = true;
        }

        public void OnGUI()
        {
            //EditorGUILayout.PropertyField(dialogueGraphProperty, new GUIContent("Graph"));
            convertFromGraph = EditorGUILayout.Toggle("Convert from graph", convertFromGraph);
            
            if (convertFromGraph)
            {
                DrawGraphConverter();
            }
            else
            {
                DrawFileConverter();
            }
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Choose directory for new dialogue file", GUILayout.MaxWidth(250)))
            {
                outputDirectory.ChooseDirectory();
            }
            EditorGUILayout.SelectableLabel(outputDirectory.PathInUnity, EditorStyles.label);
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Convert", GUILayout.MaxWidth(250), GUILayout.Height(50)))
            {
                if (convertFromGraph)
                {
                    ConvertFromGraph();
                }
                else
                {
                    Convert();
                }
            }
            
//            if (GUILayout.Button("Convert from graph", GUILayout.MaxWidth(250), GUILayout.Height(50)))
//            {
//                ConvertFromGraph();
//            }

            //Repaint();
            //EditorGUILayout.Toggle();
        }

        private void DrawFileConverter()
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Choose file to convert", GUILayout.MaxWidth(250)))
            {
                fileToConvert.ChooseFile();    
            }
            EditorGUILayout.SelectableLabel(fileToConvert.PathInUnity, EditorStyles.label);
            EditorGUILayout.EndHorizontal();
        }

        private void DrawGraphConverter()
        {
            dialogueGraph = EditorGUILayout.ObjectField(new GUIContent("Graph "), dialogueGraph, typeof(DialogueGraph), false) as DialogueGraph;
        }

        private void Convert()
        {
            if(!isInit)
                Init();
            string text = textFileLoader.LoadTextFromFile(fileToConvert.CurrentPath);
            Dialogue dialogue = ScriptScriptOperation.Convert(text);
            Debug.Log("Successfully created dialogue \"" + dialogue.Title + "\"\n" + dialogue);
            dialogueLocalizationOperation.SaveDialogueTranslations(dialogue);
            CreateAssetDialogueData(dialogue);
        }

        private void ConvertFromGraph()
        {
            if (dialogueGraph == null) return;
            DialogueNodeGraphConverter converter = new DialogueNodeGraphConverter(dialogueGraph);
            Dialogue dialogue = converter.Convert();
            dialogueLocalizationOperation.SaveDialogueTranslations(dialogue);
            CreateAssetDialogueData(dialogue);
        }

        private void CreateAssetDialogueData(Dialogue dialogue)
        {
            DialogueData data = CreateInstance<DialogueData>();
            data.Dialogue = dialogue;
            Debug.Log("Title: " + outputDirectory.PathInUnity + "/" + dialogue.Title + ".asset");
            string fileName = convertFromGraph ? dialogue.Title : fileToConvert.FileNameWithoutExtension;
            AssetDatabase.CreateAsset(data, outputDirectory.PathInUnity + "/" + fileName + ".asset");
        }
    }
}
#endif