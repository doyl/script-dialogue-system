﻿#if UNITY_EDITOR
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes;
using UnityEngine;
using XNodeEditor;

namespace Doyl.ScriptDialogueSystem.Editor.ScriptScript
{
    [NodeEditor.CustomNodeEditorAttribute(typeof(LineNode))]
    public class LineNodeEditor : NodeEditor
    {
        private LineNode lineNode;
    
        public override void OnBodyGUI()
        {
            base.OnBodyGUI();
            lineNode = (LineNode)target;
            DialogueGraph graph = (DialogueGraph)lineNode.graph;
            serializedObject.Update();
        
            lineNode.SelectedIndex = UnityEditor.EditorGUILayout.Popup("Actor", lineNode.SelectedIndex, graph.actors.ToArray());
            lineNode.actor = graph.actors[lineNode.SelectedIndex];
            lineNode.lineContent = UnityEditor.EditorGUILayout.TextArea(lineNode.lineContent, GUILayout.Height(40));
        }
    }
}
#endif