﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes;
using UnityEditor;
using XNodeEditor;

namespace Doyl.ScriptDialogueSystem.Editor.ScriptScript
{
    [CustomNodeEditor(typeof(TriggerNode))]
    public class TriggerNodeEditor : NodeEditor
    {
        private TriggerNode triggerNode;
    
        public override void OnBodyGUI()
        {
            base.OnBodyGUI();
            triggerNode = (TriggerNode)target;
            DialogueGraph graph = (DialogueGraph)triggerNode.graph;
            serializedObject.Update();
        
            triggerNode.selectedIndex = UnityEditor.EditorGUILayout.Popup("Triggers", triggerNode.selectedIndex, graph.triggers.Select(t => t.triggerName).ToArray());
            triggerNode.triggerName = graph.triggers[triggerNode.selectedIndex].triggerName;
            ArgumentsUI(graph.triggers[triggerNode.selectedIndex].argumentDefinitions);
            //base.OnBodyGUI();
        }

        private void ArgumentsUI(List<NodeEditorArgumentDefinition> arguments)
        {
            if (triggerNode.argumentsValues.Count != arguments.Count)
            {
                triggerNode.argumentsValues =
                    new List<string>(new string[arguments.Count]);
            }
            for (int i=0; i<arguments.Count; ++i)
            {
                var arg = arguments[i];
                if (arg.variableType == ArgumentType.Int)
                {
                    triggerNode.argumentsValues[i] = EditorGUILayout.IntField(arg.name, int.Parse(triggerNode.argumentsValues[i])).ToString();
                }
                if (arg.variableType == ArgumentType.String)
                {
                    triggerNode.argumentsValues[i] = EditorGUILayout.TextField(arg.name, triggerNode.argumentsValues[i]);
                }
            }
        }
    }
}

#endif