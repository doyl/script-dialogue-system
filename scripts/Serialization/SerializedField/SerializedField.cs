﻿using System;

namespace Doyl.Utility.Serialization.SerializedField
{
    public class SerializedField<T> : UnityEngine.ISerializationCallbackReceiver
    {
        [UnityEngine.SerializeField] private string serializedJson = null;
        [UnityEngine.SerializeField] private string typeName = null;
        [NonSerialized] public T item;

        public void OnAfterDeserialize()
        {
            item = default;

            var type = TypeCache.FindType(typeName);
            if (type == null)
            {
                UnityEngine.Debug.LogWarning("Serialized list error: Type '" + typeName + "' not found. Falling back to base type '" + typeof(T).FullName + "'");
                type = typeof(T);
            }

            item = (T)UnityEngine.JsonUtility.FromJson(serializedJson, type);
        }

        public void OnBeforeSerialize()
        {
            if (item == null)
            {
                typeName = null;
                serializedJson = null;
            }
            else
            {
                typeName = item.GetType().FullName;
                serializedJson = UnityEngine.JsonUtility.ToJson(item);
            }
        }
    }
}
