﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Doyl.Utility.Serialization.SerializedList
{
    [Serializable]
    public class SerializedList<T> : ISerializationCallbackReceiver
    {
        [SerializeField] private List<string> serializedJson = null;
        [SerializeField] public List<string> types = null;
        [NonSerialized] public List<T> list = new List<T>();

        public void OnBeforeSerialize()
        {
            if (serializedJson == null)
            {
                serializedJson = new List<string>();
            }
            else
            {
                serializedJson.Clear();
            }

            if (types == null)
            {
                types = new List<string>();
            }
            else
            {
                types.Clear();
            }

            foreach (var wrap in list)
            {
                if (wrap == null)
                {
                    types.Add(null);
                    serializedJson.Add(null);
                }
                else
                {
                    types.Add(wrap.GetType().FullName);
                    serializedJson.Add(JsonUtility.ToJson(wrap));
                }
            }
        }

        public void OnAfterDeserialize()
        {
            list.Clear();

            for (int i = 0; i < serializedJson.Count && i < types.Count; i++)
            {
                var typeName = types[i];
                var jsonData = serializedJson[i];

                var type = TypeCache.FindType(typeName);
                if (type == null)
                {
                    Debug.LogWarning("Serialized list error: Type '" + typeName + "' not found. Falling back to base type '" + typeof(T).FullName + "'");
                    type = typeof(T);
                }

                T item = (T)JsonUtility.FromJson(jsonData, type);
                list.Add(item);
            }
        }
    }
}
