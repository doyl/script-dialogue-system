﻿using System;
using System.Collections.Generic;

namespace Doyl.Utility.Serialization
{
    public class TypeCache
    {
        static Dictionary<string, Type> cache;

        public static Type FindType(string name)
        {
            Type type;
            if (cache != null && cache.TryGetValue(name, out type))
            {
                return type;
            }

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = assembly.GetType(name);
                if (type != null)
                {
                    if (cache == null)
                    {
                        cache = new Dictionary<string, Type>();
                    }
                    cache[name] = type;
                    return type;
                }
            }

            return null;
        }
    }
}
