﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Handling;
using Doyl.ScriptDialogueSystem.Scripts.LocalizationSystem;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem
{
    public class ScriptScriptDependencyProvider
    {
        private static ScriptScriptDependencyProvider scriptScriptDependencyProvider;
        public DialogueLocalizationOperation DialogueLocalizationOperation { get; private set; }
        public LineTranslator LineTranslator { get; private set; }
        public DecisionSetTranslator DecisionSetTranslator { get; private set; }
        public Localization Localization { get; private set; }
        public CsvLoader CsvLoader { get; private set; }
        public DialogueConfiguration DialogueConfiguration { get; private set; }

        public static ScriptScriptDependencyProvider GetSingleton()
        {
            return scriptScriptDependencyProvider ??
                   (scriptScriptDependencyProvider = new ScriptScriptDependencyProvider());
        }
        
        public ScriptScriptDependencyProvider()
        {
            DialogueConfiguration = new DialogueConfiguration();
            CsvLoader = new CsvLoader();
            Localization = new Localization(CsvLoader);
            LineTranslator = new LineTranslator();
            DecisionSetTranslator = new DecisionSetTranslator();
            DialogueLocalizationOperation = new DialogueLocalizationOperation(Localization, DialogueConfiguration, LineTranslator, DecisionSetTranslator);
        }
    }
}