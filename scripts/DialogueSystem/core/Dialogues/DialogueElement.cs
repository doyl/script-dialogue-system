﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues
{
    [Serializable]
    public abstract class DialogueElement
    {
        [UnityEngine.SerializeField] private string id;
        public ConditionUsage condition;
        public Guid Id
        {
            get => string.IsNullOrEmpty(id) ? Guid.Empty : Guid.Parse(id);
            set => id = value.ToString();
        }

        public virtual void Accept(DialogueElementsVisitor visitor) {}

        protected DialogueElement()
        {
            Id = Guid.NewGuid();
        }
        
        protected DialogueElement(Guid id)
        {
            Id = id;
        }
        
        public bool CheckCondition(List<DialogueCondition> possibleConditions)
        {
            if (condition==null || string.IsNullOrEmpty(condition.Name))
                return true;
            DialogueCondition targetCondition = possibleConditions.Find(t => t.Name == condition.Name);
            if (targetCondition == null)
            {
                string possibleConditionsNames = "";
                possibleConditions.ForEach(t => possibleConditionsNames += (t.Name + "\n"));
                throw new Exception("Can't find " + condition.Name + " on list of conditions! \n Possible list of conditions: \n" + possibleConditionsNames);
            }
            return targetCondition.IsFulfilled(condition.arguments.list) ^ condition.isNegated;
        }
    }
}