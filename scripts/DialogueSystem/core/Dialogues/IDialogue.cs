﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues
{
    public interface IDialogue
    {
        List<DialogueCondition> Conditions { get; }
        List<DialogueVariable> Variables { get; }
        DialogueElement NextElement();
        void Next();
        void JumpTo(Jump jump);
        void InvokeTrigger(string triggerName, List<Argument> arguments);
    }
}
