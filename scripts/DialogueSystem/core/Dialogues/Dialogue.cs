﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Actors;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.utility.Serialization;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues
{
    [System.Serializable]
    public class Dialogue
    {
        [UnityEngine.SerializeField] private string id;
        [UnityEngine.SerializeField] private string title;
        private Dictionary<string, Actor> actors;
        private readonly Dictionary<string, Tag> tags;
        [UnityEngine.SerializeField] private List<DialogueCondition> conditions = new List<DialogueCondition>();
        [UnityEngine.SerializeField] private List<DialogueTrigger> triggers = new List<DialogueTrigger>();
        [UnityEngine.SerializeField] private DialogueElementsSerializedList dialogueElements = new DialogueElementsSerializedList();
        [UnityEngine.SerializeField] private List<DialogueVariable> variables = new List<DialogueVariable>();

        public Guid Id
        {
            get => string.IsNullOrEmpty(id) ? Guid.Empty : Guid.Parse(id);
            private set => id = value.ToString();
        }

        public string Title 
        { 
            get => title;
            private set => title = value;
        }
        public List<DialogueCondition> Conditions 
        { 
            get => conditions;
            private set => conditions = value;
        }
        public List<DialogueTrigger> Triggers 
        { 
            get => triggers;
            private set => triggers = value;
        }
        public DialogueElementsSerializedList DialogueElements => dialogueElements;

        public List<DialogueVariable> Variables 
        { 
            get => variables;
            private set => variables = value;
        }

        public Dialogue(Guid id, string title, Dictionary<string, Actor> actors, Dictionary<string, Tag> tags, Dictionary<string, DialogueVariable> variables,
            Dictionary<string, DialogueCondition> conditions, Dictionary<string, DialogueTrigger> triggers, List<DialogueElement> elements)
        {
            Id = id;
            Title = title;
            this.actors = actors;
            this.tags = tags;
            foreach (DialogueVariable variable in variables.Values)
            {
                Variables.Add(variable);
            }
            foreach (DialogueCondition condition in conditions.Values)
            {
                Conditions.Add(condition);
            }
            foreach (DialogueTrigger trigger in triggers.Values)
            {
                Triggers.Add(trigger);
            }
            DialogueElements.list = elements;
        }
        
        public Dialogue(string title, List<Actor> actors, List<Tag> tags, List<DialogueVariable> variables,
            List<DialogueCondition> conditions, List<DialogueTrigger> triggers, List<DialogueElement> elements)
        {
            Title = title;
            this.actors = actors.ToDictionary(x => x.Name);
            this.tags = tags.ToDictionary(x => x.Name);
            Variables = variables;
            Conditions = conditions;
            Triggers = triggers;
            DialogueElements.list = elements;
        }

        public override string ToString()
        {
            string result = "Title: " + title + "\n";
            result += "\n Tags: ";
            foreach (Tag tag in tags.Values)
            {
                result = result + tag.Name + " ";
            }
            return result;
        }
    }
}
