﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Handling;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues
{
    public class DialogueInstance : IDialogue
    {
        private Dialogue DialogueData { get; }
        private readonly DialogueElementsVisitor visitor;
        readonly List<DialogueTrigger> triggersInstance;
        readonly List<DialogueCondition> conditionsInstance;
        private readonly List<DialogueVariable> variables;
        private int currentElementIndex;
        private int nextElementIndex;
        
        public List<DialogueCondition> Conditions => conditionsInstance;
        public List<DialogueVariable> Variables => variables;

        public DialogueElement GetCurrentElement => DialogueData.DialogueElements.list[currentElementIndex];
        
        public DialogueElement GetNextElement => DialogueData.DialogueElements.list[nextElementIndex];

        public DialogueInstance(Dialogue dialogue, IDialogueElementsReceiver elementsReceiver)
        {
            LineTranslator lineTranslator = ScriptScriptDependencyProvider.GetSingleton().LineTranslator;
            DecisionSetTranslator decisionSetTranslator =
                ScriptScriptDependencyProvider.GetSingleton().DecisionSetTranslator;
            DialogueLocalizationOperation dialogueLocalizationOperation =
                ScriptScriptDependencyProvider.GetSingleton().DialogueLocalizationOperation;
            DialogueData = dialogue;
            variables = DialogueData.Variables.ConvertAll(t => t.Copy());
            triggersInstance = DialogueData.Triggers.ConvertAll(t => t.Copy()); 
            conditionsInstance = DialogueData.Conditions.ConvertAll(t => t.Copy());
            visitor = new DialogueElementsProcessVisitor(elementsReceiver, this, lineTranslator, decisionSetTranslator, dialogueLocalizationOperation); //TODO: Line Translator as dependency
            elementsReceiver.Load(this);
        }

        public DialogueElement NextElement()
        {
            if (DialogueData.DialogueElements.list[currentElementIndex] is DecisionSet)
            {
                throw new Exception("Trying to get next dialogue element on DecisionBlock! You should choose a decision instead!");
            }

            return SimpleNextElement();
        }

        private DialogueElement SimpleNextElement()
        {
            if (nextElementIndex >= DialogueData.DialogueElements.list.Count)
            {
                EndDialogue();
                return null;
            }
            DialogueElement element = DialogueData.DialogueElements.list[nextElementIndex];
            currentElementIndex = nextElementIndex;
            ++nextElementIndex;
            if (!element.CheckCondition(Conditions))
                return SimpleNextElement();
            return element;
        }

        public void Next()
        {
            NextElement()?.Accept(visitor);
        }

        private void SimpleNext()
        {
            SimpleNextElement()?.Accept(visitor);
        }

        public void JumpTo(Jump jump)
        {
            if (jump.DestinationTagName == "End")
            {
                EndDialogue();
                return;
            }
            JumpToIndex(jump.DestinationIndex);
            SimpleNext();
        }

        public void MakeDecision(Decision decision)
        {
            JumpTo(decision.jump);
        }

        public void FillCondition(string conditionName, DialogueConditionOutside conditionOutside)
        {
            DialogueCondition condition = conditionsInstance.Find(t => t.Name == conditionName);
            if (condition == null)
            {
                Debug.LogWarning("Condition " + conditionName + " was not found in conditions list.");
                return;
            }

            condition.Fill(conditionOutside);
        }

        public void FillCondition(string name, Func<bool> conditionFunction)
        {
            DialogueConditionOutside conditionOutside = new Condition(conditionFunction);
            FillCondition(name, conditionOutside);
        }

        public void FillCondition<T>(string name, Func<T, bool> conditionFunction)
        {
            DialogueConditionOutside conditionOutside = new Condition<T>(conditionFunction);
            FillCondition(name, conditionOutside);
        }
        public void FillCondition<T1, T2>(string name, Func<T1, T2, bool> conditionFunction)
        {
            DialogueConditionOutside conditionOutside = new Condition<T1, T2>(conditionFunction);
            FillCondition(name, conditionOutside);
        }

        public void SubscribeToTrigger(string triggerName, Action fun)
        {
            DialogueTrigger trigger = triggersInstance.Find(t => t.Name == triggerName);
            trigger?.Subscribe(fun);
        }

        public void SubscribeToTrigger<T>(string name, Action<T> fun)
        {
            DialogueTrigger trigger = triggersInstance.Find(t => t.Name == name);
            trigger?.Subscribe(fun);
        }

        public void SubscribeToTrigger<T1, T2>(string name, Action<T1, T2> fun)
        {
            DialogueTrigger trigger = triggersInstance.Find(t => t.Name == name);
            trigger?.Subscribe(fun);
        }

        private void EndDialogue()
        {
            visitor.Finish();
        }

        public void InvokeTrigger(string triggerName, List<Argument> arguments)
        {
            DialogueTrigger trigger = triggersInstance.Find(x => x.Name == triggerName);
            trigger.Invoke(arguments);
            SimpleNext();
        }

        private void JumpToIndex(int index)
        {
            nextElementIndex = index;
        }

        private void FillVariable(string name, DialogueVar dialogueVar)
        {
            DialogueVariable variable = variables.Find(t => t.Name == name);
            if (variable == null)
            {
                Debug.LogWarning("Variable " + name + " was not found in variables list.");
                return;
            }
            variable.Fill(dialogueVar);
        }

        public void FillVariable(string name, Func<string> variable)
        {
            DialogueVar var = new DialogueVar(variable);
            FillVariable(name, var);
        }

        public void FillVariable<T>(string name, Func<string> variable)
        {
            DialogueVar var = new DialogueVar(() => variable.Invoke().ToString());
            FillVariable(name, var);
        }

        public void FillVariable<T1, T2>(string name, Func<T1, T2, string> variable)
        {
            DialogueVar var = new DialogueVar(() => "");//variable.Invoke().ToString()); //TODO: Fix this
            FillVariable(name, var);
        }

        public void FillVariable(string name, string variable)
        {
            DialogueVar var = new DialogueVar(() => variable);
            FillVariable(name, var);
        }
    }
}
