﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Handling;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues
{
    [System.Serializable]
    public class DialogueData : ScriptableObject
    {
        [SerializeField] private Dialogue dialogue;
        public Dialogue Dialogue { get { return dialogue; } set { dialogue = value; } }

        public DialogueInstance LoadDialogue(IDialogueElementsReceiver dialogueReceiver)
        {
            return new DialogueInstance(dialogue, dialogueReceiver);
        }
    }
}