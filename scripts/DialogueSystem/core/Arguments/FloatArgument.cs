﻿namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments
{
    public class FloatArgument : Argument
    {
        public float Value { get; }
        public FloatArgument(float value)
        {
            Value = value;
        }
    }
}
