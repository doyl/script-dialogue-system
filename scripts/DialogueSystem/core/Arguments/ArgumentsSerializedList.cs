﻿using Doyl.Utility.Serialization.SerializedList;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments
{
    [System.Serializable]
    public class ArgumentsSerializedList : SerializedList<Argument>
    {
    }
}
