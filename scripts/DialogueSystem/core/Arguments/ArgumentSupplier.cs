﻿using System;
using System.Collections.Generic;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments
{
    public class ArgumentSupplier
    {
        private readonly List<Argument> arguments;
        private int currentPosition = 0;

        public ArgumentSupplier(List<Argument> arguments)
        {
            this.arguments = arguments;
        }

        public T Receive<T>()
        {
            T result;
            Type type = typeof(T);
            if (type == typeof(int))
            {
                result = (T) Convert.ChangeType(ReceiveInt(), typeof(T));
                return result;
            }

            if (type == typeof(string))
            {
                result = (T) Convert.ChangeType(ReceiveString(), typeof(T));
                return result;
            }

            if (type == typeof(float))
            {
                result = (T) Convert.ChangeType(ReceiveFloat(), typeof(T));
                return result;
            }
            throw new Exception("Unrecognized type given to Trigger");
        }

        public int ReceiveInt()
        {
            return ReceiveInt(arguments[currentPosition++]);
        }

        public string ReceiveString()
        {
            return ReceiveString(arguments[currentPosition++]);
        }

        public float ReceiveFloat()
        {
            return ReceiveFloat(arguments[currentPosition++]);
        }

        public static int ReceiveInt(Argument argument)
        {
            IntArgument intArgument = (IntArgument)argument;
            return intArgument.Value;
        }

        public static string ReceiveString(Argument argument)
        {
            StringArgument stringArgument = (StringArgument) argument;
            return stringArgument.Value;
        }

        public static float ReceiveFloat(Argument argument)
        {
            float value;
            if (argument is FloatArgument floatArgument)
            {
                value = floatArgument.Value;
            }
            else
            {
                value = ((IntArgument)argument).Value;
            }
            return value;
        }
    }
}
