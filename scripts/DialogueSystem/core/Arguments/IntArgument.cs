﻿using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments
{
    public class IntArgument : Argument
    {
        [SerializeField] private int value;
        public int Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public IntArgument(int value)
        {
            Value = value;
        }
    }
}
