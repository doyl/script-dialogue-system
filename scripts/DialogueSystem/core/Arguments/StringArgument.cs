﻿using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments
{
    public class StringArgument : Argument
    {
        [SerializeField] private string value;
        public string Value => value;

        public StringArgument(string value)
        {
            this.value = value;
        }
    }
}
