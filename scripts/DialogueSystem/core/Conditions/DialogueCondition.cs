﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions
{
    [System.Serializable]
    public class DialogueCondition : DialogueElement
    {
        [UnityEngine.SerializeField] private string name;
        public string Name { 
            get => name;
            private set => name = value;
        }
        public new DialogueConditionOutside condition;

        public DialogueCondition(string name)
        {
            Name = name;
        }

        public void Fill(DialogueConditionOutside condition)
        {
            this.condition = condition;
        }

        public bool IsFulfilled(List<Argument> arguments)
        {
            return condition == null || condition.CheckCondition(arguments);
        }

        public DialogueCondition Copy()
        {
            return new DialogueCondition(Name);
        }
    }
}