﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions
{
    public abstract class DialogueConditionOutside
    {
        public abstract bool CheckCondition(List<Argument> arguments);
    }
}
