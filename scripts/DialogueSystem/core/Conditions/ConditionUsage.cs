﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions
{
    [System.Serializable]
    public class ConditionUsage
    {
        [UnityEngine.SerializeField] private string name;
        public string Name { get { return name; } private set { name = value; } }
        public bool isNegated;
        public ArgumentsSerializedList arguments = new ArgumentsSerializedList();
        
        public ConditionUsage(string name, bool isNegated, List<Argument> arguments)
        {
            //Debug.Log("Creating condition usage with name: " + name);
            Name = name;
            this.isNegated = isNegated;
            this.arguments.list = arguments;
        }
    }
}