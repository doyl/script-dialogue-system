﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions
{
    public class Condition : DialogueConditionOutside
    {
        private readonly Func<bool> function;
        
        public Condition(Func<bool> function)
        {
            this.function = function;
        }
        
        public override bool CheckCondition(List<Argument> arguments)
        {
            if (arguments != null && arguments.Count != 0)
            {
                throw new Exception("Incorrect number of expected arguments!");
            }
            return function.Invoke();
        }
    }

    public class Condition<T> : DialogueConditionOutside
    {
        private readonly Func<T, bool> function;

        public Condition(Func<T, bool> function)
        {
            this.function = function;
        }

        public override bool CheckCondition(List<Argument> arguments)
        {
            if (arguments.Count != 1)
            {
                throw new Exception("Incorrect number of expected arguments!");
            }
            ArgumentSupplier argumentSupplier = new ArgumentSupplier(arguments);
            T argument = argumentSupplier.Receive<T>();
            return function.Invoke(argument);
        }
    }
    
    public class Condition<T1, T2> : DialogueConditionOutside
    {
        private readonly Func<T1, T2, bool> function;
        
        public Condition(Func<T1, T2, bool> function)
        {
            this.function = function;
        }
        public override bool CheckCondition(List<Argument> arguments)
        {
            if (arguments.Count != 2)
            {
                throw new Exception("Incorrect number of expected arguments!");
            }
            ArgumentSupplier argumentSupplier = new ArgumentSupplier(arguments);
            T1 argument1 = argumentSupplier.Receive<T1>();
            T2 argument2 = argumentSupplier.Receive<T2>();
            return function.Invoke(argument1, argument2);
        }
    }
}
