﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps
{
    [System.Serializable]
    public class Tag
    {
        [SerializeField] private string name;
        public string Name { get { return name; } set { name = value; } }
        [SerializeField] private DialogueElement dialogueElement;
        public DialogueElement DialogueElement { get; set; }
        [SerializeField] int correspondingElementIndex;

        public Tag(string name, DialogueElement correspondingElement)
        {
            Name = name;
            DialogueElement = correspondingElement;
        }
    }
}