﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps
{
    [System.Serializable]
    public class Jump : DialogueElement
    {
        [SerializeField] private string destinationTagName;
        public string DestinationTagName { get { return destinationTagName; } set { destinationTagName = value; } }
        [SerializeField] private int destinationIndex;
        public int DestinationIndex { get { return destinationIndex; } set { destinationIndex = value; } }

        public Jump(string destinationTagName)
        {
            DestinationTagName = destinationTagName;
        }

        public override void Accept(DialogueElementsVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
