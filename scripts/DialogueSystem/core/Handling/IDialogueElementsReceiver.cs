﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Handling
{
    public interface IDialogueElementsReceiver
    {
        void Load(DialogueInstance dialogueInstance);
        void Load(DecisionSet decisionSet);
        void Load(DialogueLine line);
        void OnFinish();
    }
}
