﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Handling
{
    public class DialogueElementsProcessVisitor : DialogueElementsVisitor
    {
        private readonly IDialogueElementsReceiver elementsReceiver;
        private readonly IDialogue dialogue;
        private readonly LineTranslator lineTranslator;
        private readonly DecisionSetTranslator decisionSetTranslator;
        private readonly DialogueLocalizationOperation dialogueLocalizationOperation;
        
        public DialogueElementsProcessVisitor(IDialogueElementsReceiver elementsReceiver, IDialogue dialogue,
            LineTranslator lineTranslator, DecisionSetTranslator decisionSetTranslator,
            DialogueLocalizationOperation dialogueLocalizationOperation)
        {
            this.elementsReceiver = elementsReceiver;
            this.dialogue = dialogue;
            this.lineTranslator = lineTranslator;
            this.decisionSetTranslator = decisionSetTranslator;
            this.dialogueLocalizationOperation = dialogueLocalizationOperation;
        }

        public override void Visit(DialogueLine line)
        {
            line = lineTranslator.Translate(line, dialogueLocalizationOperation);
            line.possibleVariables = dialogue.Variables;
            elementsReceiver.Load(line);
        }

        public override void Visit(DecisionSet decisionSet)
        {
            decisionSet = decisionSetTranslator.Translate(decisionSet, dialogueLocalizationOperation);
            decisionSet.PassVariables(dialogue.Variables);
            decisionSet.PassConditions(dialogue.Conditions);
            elementsReceiver.Load(decisionSet);
        }

        public override void Visit(DialogueTriggerInvocation invocation)
        {
            dialogue.InvokeTrigger(invocation.TriggerName, invocation.arguments.list);
        }

        public override void Visit(Jump jump)
        {
            dialogue.JumpTo(jump);
        }

        public override void Finish()
        {
            elementsReceiver.OnFinish();
        }
    }
}
