﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Converter;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Handling
{
    public class DecisionSetTranslator
    {
        public void SaveTranslation(DecisionSet decisionSet,
            DialogueLocalizationOperation dialogueLocalizationOperation, string languageCode)
        {
            for (int i = 0; i < decisionSet.Decisions.Count; ++i)
            {
                dialogueLocalizationOperation.SaveLocalization(
                    decisionSet.Id + "/" + i, 
                    decisionSet.Decisions[i].DecisionContent.GetRawLine(), languageCode);
            }
        }
        
        public DecisionSet Translate(DecisionSet decisionSet, DialogueLocalizationOperation dialogueLocalizationOperation)
        {
            for (int i = 0; i < decisionSet.Decisions.Count; ++i)
            {
                string decisionContent = dialogueLocalizationOperation.LoadLocalization(decisionSet.Id + "/" + i);
                decisionSet.Decisions[i].DecisionContent = ScriptScriptOperation.ConvertLine(decisionContent);
            }

            return decisionSet;
        }
    }
}