﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines
{
    [Serializable]
    public class DialogueLine : DialogueElement
    {
        [SerializeField] private string actorName;
        public string ActorName 
        { 
            get => actorName;
            set => actorName = value;
        }

        [SerializeField] private LineContent lineContent;
        public LineContent LineContent 
        { 
            get => lineContent;
            set => lineContent = value;
        }

        public List<DialogueVariable> possibleVariables;
        
        public DialogueLine(Guid id, LineContent lineContent, string actor) : base(id)
        {
            ActorName = actor;
            LineContent = lineContent;
        }

        private string GetLineText(List<DialogueVariable> possibleVariables)
        {
            return LineContent.GetValue(possibleVariables);
        }

        public string GetLine()
        {
            return GetLineText(possibleVariables);
        }

        public override void Accept(DialogueElementsVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
