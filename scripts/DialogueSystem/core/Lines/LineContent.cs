﻿using System.Collections.Generic;
using System.Text;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines
{
    [System.Serializable]
    public class LineContent
    {
        [SerializeField] private List<string> lineParts;
        [SerializeField] private string line; 
        [SerializeField] private List<DialogueVariableInstance> variables;

        public LineContent(List<string> lineParts, List<DialogueVariableInstance> variables)
        {
            this.lineParts = lineParts;
            StringBuilder finalLine = new StringBuilder();
            foreach (string linePart in lineParts)
            {
                finalLine.Append(linePart);
            }

            line = finalLine.ToString();
            this.variables = variables;
        }

        public LineContent(string line, List<DialogueVariableInstance> variables)
        {
            this.line = line;
            this.variables = variables;
        }

        public LineContent(string line)
        {
            lineParts = new List<string> {line};
        }

        public string GetRawLine()
        {
            return line;
        }
        
        public string GetValue(List<DialogueVariable> possibleVariables)
        {
            int variableIterator = 0;
            StringBuilder lineContent = new StringBuilder();
            foreach (string part in lineParts)
            {
                if (part[0].Equals('{'))
                {
                    if (variables != null && variableIterator != variables.Count)
                    {
                        lineContent.Append(variables[variableIterator].GetValue(possibleVariables));
                        ++variableIterator;
                    }
                }
                else
                {
                    lineContent.Append(part);
                }
            }
            return lineContent.ToString();
        }
    }
}