﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Converter;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core
{
    public class LineTranslator
    {
        public void SaveTranslation(DialogueLine dialogueLine, DialogueLocalizationOperation dialogueLocalizationOperation, string languageCode)
        {
            string key = dialogueLine.Id.ToString();
            string translation = dialogueLine.LineContent.GetRawLine();
            dialogueLocalizationOperation.SaveLocalization(key, translation, languageCode);
        }
    
        public DialogueLine Translate(DialogueLine dialogueLine, DialogueLocalizationOperation dialogueLocalizationOperation)
        {
            string lineContent = dialogueLocalizationOperation.LoadLocalization(dialogueLine.Id.ToString());
            dialogueLine.LineContent = ScriptScriptOperation.ConvertLine(lineContent); 
            return dialogueLine;
        }
    }
}
