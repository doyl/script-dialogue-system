﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers
{
    [System.Serializable]
    public class DialogueTriggerInvocation : DialogueElement
    {
        public string TriggerName;
        public ArgumentsSerializedList arguments;

        public DialogueTriggerInvocation(string triggerName, List<Argument> arguments)
        {
            this.TriggerName = triggerName;
            this.arguments = new ArgumentsSerializedList {list = arguments};
        }

        public override void Accept(DialogueElementsVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}