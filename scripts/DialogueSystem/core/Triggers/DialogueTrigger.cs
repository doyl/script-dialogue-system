﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers
{
    [Serializable]
    public class DialogueTrigger
    {
        [SerializeField] string name;
        public string Name { get { return name; } set { name = value; } }
        private readonly ArgumentType[] awaitedArgumentTypes;
        public ArgumentType[] AwaitedArgumentTypes => awaitedArgumentTypes;
        
        public delegate void TriggerDelegate(List<Argument> arguments);
        public event TriggerDelegate OnTriggerTriggered;

        public void Invoke(List<Argument> arguments)
        {
            OnTriggerTriggered?.Invoke(arguments);
            //onTriggerTriggeredSubject.OnNext(arguments);
        }

        public void Subscribe(Action fun)
        {
            OnTriggerTriggered += new TriggerDelegate(_ => fun.Invoke());
        }

        public void Subscribe<T>(Action<T> fun)
        {
            //OnTriggerTriggered += new TriggerDelegate(fun);
            FunctionInvoker<T> invoker = new FunctionInvoker<T>();
            invoker.Subscribe(fun);
            //OnTriggerTriggeredStream.Subscribe(x => invoker.Invoke(x));
            OnTriggerTriggered += invoker.Invoke;
        }

        public void Subscribe<T1, T2>(Action<T1, T2> fun)
        {
            FunctionInvoker<T1, T2> invoker = new FunctionInvoker<T1, T2>();
            invoker.Subscribe(fun);
            //OnTriggerTriggeredStream.Subscribe(x => invoker.Invoke(x));
            OnTriggerTriggered += invoker.Invoke;
        }

        public DialogueTrigger(string name, params ArgumentType[] awaitedArguments)
        {
            Name = name;
            awaitedArgumentTypes = awaitedArguments;
        }
        
        public DialogueTrigger Copy()
        {
            return new DialogueTrigger(name, awaitedArgumentTypes);
        }
    }
}