﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers
{
    public class FunctionInvoker<T>
    {
//        private readonly Subject<T> onFunctionInvokedSubject = new Subject<T>();
//        private UniRx.IObservable<T> OnFunctionInvokedStream => onFunctionInvokedSubject.AsObservable();

        delegate void OnFunctionInvoked(T arguments);

        private OnFunctionInvoked onFunctionInvoked;
        
        public void Invoke(List<Argument> arguments)
        {
            ArgumentSupplier supplier = new ArgumentSupplier(arguments);
            //onFunctionInvokedSubject.OnNext(supplier.Receive<T>());
            onFunctionInvoked?.Invoke(supplier.Receive<T>());
        }

        public void Subscribe(Action<T> function)
        {
            //OnFunctionInvokedStream.Subscribe(function.Invoke);
            onFunctionInvoked += function.Invoke;
        }
    }

    public class FunctionInvoker<T1, T2>
    {
        public delegate void OnFunctionCalledDelegate(T1 first, T2 second);
        public event OnFunctionCalledDelegate OnFunctionCalled;
        
        public void Invoke(List<Argument> arguments)
        {
            ArgumentSupplier supplier = new ArgumentSupplier(arguments);
            OnFunctionCalled?.Invoke(supplier.Receive<T1>(), supplier.Receive<T2>());
        }

        public void Subscribe(Action<T1, T2> function)
        {
            OnFunctionCalled += function.Invoke;
        }
    }
}
