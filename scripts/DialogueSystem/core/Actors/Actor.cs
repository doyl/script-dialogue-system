﻿namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Actors
{
    public class Actor
    {
        public string Name { get; }
        public Actor(string name)
        {
            Name = name;
        }
    }
}