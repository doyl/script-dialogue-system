﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions
{
    [Serializable]
    public class DecisionSet : DialogueElement
    {
        [SerializeField] private List<Decision> decisions;
        private List<DialogueCondition> possibleConditions;
        public List<Decision> Decisions { get { return decisions; } private set { decisions = value; } }

        public DecisionSet(Guid id, List<Decision> decisions) : base(id)
        {
            Decisions = decisions;
        }

        public void PassConditions(List<DialogueCondition> conditions)
        {
            possibleConditions = conditions;
        }
        public List<Decision> GetPossibleDecisions()
        {
            return decisions.FindAll(t => t.CheckConditions(possibleConditions));
        }

        public override void Accept(DialogueElementsVisitor visitor)
        {
            visitor.Visit(this);
        }

        public void PassVariables(List<DialogueVariable> dialogueVariables)
        {
            foreach (Decision dec in decisions)
            {
                dec.possibleVariables = dialogueVariables;
            }
        }
    }
}