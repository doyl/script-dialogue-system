﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions
{
    [System.Serializable]
    public class Decision
    {
        public LineContent DecisionContent;
        public string DestinationTagName;
        //public string conditionName;
        //private readonly bool isConditionNegated;
        public Jump jump;
        
        [SerializeField] private int destinationIndex;

        public List<DialogueVariable> possibleVariables;
        public int DestinationIndex { get { return jump.DestinationIndex; } set { jump.DestinationIndex = value; } }
        public ConditionUsage conditionUsage;

        public Decision(LineContent decisionContent, string tagNameDestination, ConditionUsage condition)
        {
            this.DecisionContent = decisionContent;
            this.DestinationTagName = tagNameDestination;
            //jump.DestinationTagName = tagNameDestination;
            jump = new Jump(tagNameDestination);
//            if (condition != null)
//            {
//                this.conditionName = condition.Name;
//                isConditionNegated = condition.isNegated;
//            }

            conditionUsage = condition;
        }

        public string GetText()
        {
            return DecisionContent.GetValue(possibleVariables);
        }

        public bool CheckConditions(List<DialogueCondition> conditionsDeclarations)
        {
            if (conditionUsage == null || string.IsNullOrEmpty(conditionUsage.Name))
                return true;
            DialogueCondition targetCondition = conditionsDeclarations.Find(t => t.Name == conditionUsage.Name);
            if (targetCondition == null)
            {
                string possibleConditions = "";
                conditionsDeclarations.ForEach(t => possibleConditions += (t.Name + "\n"));
                throw new Exception("Can't find " + conditionUsage.Name + " on list of conditions! \n Possible list of conditions: \n" + possibleConditions);
            }
            return conditionsDeclarations.Find(t => t.Name == conditionUsage.Name).IsFulfilled(conditionUsage.arguments.list) ^ conditionUsage.isNegated; //TODO: isFullfiled should be getting arguments
        }
    }
}
