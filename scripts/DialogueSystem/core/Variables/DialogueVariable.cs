﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables
{
    [System.Serializable]
    public class DialogueVariable
    {
        [UnityEngine.SerializeField] private string name;
        public string Name { get { return name; } private set { name = value; } }
        public DialogueFunctionOutside function;

        public DialogueVariable(string name)
        {
            Name = name;
        }

        public void Fill(DialogueFunctionOutside function)
        {
            this.function = function;
        }

        public string GetValue(List<Argument> arguments)
        {
            if (function == null)
                throw new Exception("Variable used, but not assigned!");

            return function.GetValue(arguments);
        }

        public DialogueVariable Copy()
        {
            return new DialogueVariable(Name);
        }
    }

    public abstract class DialogueFunctionOutside
    {
        public abstract string GetValue(List<Argument> arguments);
    }
}