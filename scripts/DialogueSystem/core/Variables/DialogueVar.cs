﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables
{
    public class DialogueVar : DialogueFunctionOutside
    {
        private readonly Func<string> function;
        
        public DialogueVar(Func<string> func)
        {
            function = func;
        }

        public override string GetValue(List<Argument> arguments)
        {
            if (arguments != null && arguments.Count != 0)
            {
                throw new Exception("Incorrect number of expected arguments!");
            }
            return function.Invoke();
        }
    }
}