﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables
{
    [Serializable]
    public class DialogueVariableInstance
    {
        [SerializeField] private string name;
        [SerializeField] private List<Argument> arguments;

        public DialogueVariableInstance(string name, List<Argument> arguments)
        {
            this.name = name;
            this.arguments = arguments;
        }

        public string GetValue(List<DialogueVariable> possibleVariables)
        {
            DialogueVariable targetVariable = possibleVariables.Find(t => t.Name == name);
            if (targetVariable == null)
            {
                string possibleConditionsNames = "";
                possibleVariables.ForEach(t => possibleConditionsNames += (t.Name + "\n"));
                throw new Exception("Can't find " + name + " on list of conditions! \n Possible list of conditions: \n" + possibleConditionsNames);
            }

            return targetVariable.GetValue(arguments);
        }

        public string GetVariableId()
        {
            return "{" + name + "}";
        }
    }
}