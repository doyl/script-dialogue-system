﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Handling;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.LocalizationSystem;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem
{
    public class DialogueLocalizationOperation : DialogueElementsVisitor
    {
        private readonly Localization localization;
        private readonly DialogueConfiguration dialogueConfiguration;
        private readonly LineTranslator lineTranslator;
        private readonly DecisionSetTranslator decisionSetTranslator;
    
        public DialogueLocalizationOperation(Localization localization,
            DialogueConfiguration dialogueConfiguration,
            LineTranslator lineTranslator,
            DecisionSetTranslator decisionSetTranslator)
        {
            this.localization = localization;
            this.dialogueConfiguration = dialogueConfiguration;
            this.lineTranslator = lineTranslator;
            this.decisionSetTranslator = decisionSetTranslator;
        }
    
        public void SaveLocalization(string key, string translation, string languageCode)
        {
            localization.Add(key, translation, languageCode);
        }

        public void SaveLocalization(string key, string translation)
        {
            SaveLocalization(key, translation, dialogueConfiguration.currentDefaultLanguageCode);
        }

        public string LoadLocalization(string key, string languageCode)
        {
            return localization.GetLocalizedValue(key, languageCode);
        }

        public string LoadLocalization(string key)
        {
            return LoadLocalization(key, dialogueConfiguration.currentDefaultLanguageCode);
        }

        public void SaveDialogueTranslations(Dialogue dialogue)
        {
            foreach (var dialogueElement in dialogue.DialogueElements.list)
            {
                dialogueElement.Accept(this);
            }
        }

        public override void Visit(DialogueLine dialogueLine)
        {
            lineTranslator.SaveTranslation(dialogueLine, this, dialogueConfiguration.currentDefaultLanguageCode);
        }

        public override void Visit(DecisionSet decisionSet)
        {
            decisionSetTranslator.SaveTranslation(decisionSet, this, dialogueConfiguration.currentDefaultLanguageCode);
        }
    }
}