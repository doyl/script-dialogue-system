﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Handling;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using UnityEngine;
using UnityEngine.Events;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.components
{
    public class DialogueElementsReceiver : MonoBehaviour, IDialogueElementsReceiver
    {
        [SerializeField] private DialogueInstanceEvent onDialogueInstanceLoaded;
        [SerializeField] private DialogueLineEvent onLineReceived;
        [SerializeField] private DialogueDecisionSetEvent onDecisionSetReceived;
        [SerializeField] private UnityEvent onDialogueFinished;
        public void Load(DialogueInstance dialogueInstance)
        {
            onDialogueInstanceLoaded.Invoke(dialogueInstance);
        }

        public void Load(DecisionSet decisionSet)
        {
            onDecisionSetReceived.Invoke(decisionSet);
        }

        public void Load(DialogueLine line)
        {
            onLineReceived.Invoke(line);
        }

        public void OnFinish()
        {
            onDialogueFinished.Invoke();
        }
    
        [System.Serializable]
        public class DialogueLineEvent : UnityEvent<DialogueLine>{}

        [System.Serializable]
        public class DialogueDecisionSetEvent : UnityEvent<DecisionSet>{}

        [System.Serializable]
        public class DialogueInstanceEvent : UnityEvent<DialogueInstance>{}
    }
}