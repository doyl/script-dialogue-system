﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers;
using Doyl.Utility.Serialization.SerializedList;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.utility.Serialization
{
    [System.Serializable]
    public class DialogueElementsSerializedList : SerializedList<DialogueElement>
    {
    }

    [System.Serializable]
    public class DialogueTriggerSerializedList : SerializedList<DialogueTrigger>
    {
    }
}
