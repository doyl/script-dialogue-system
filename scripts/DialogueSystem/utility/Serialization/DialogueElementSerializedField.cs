﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.Utility.Serialization.SerializedField;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.utility.Serialization
{
    [System.Serializable]
    public class DialogueElementSerializedField : SerializedField<DialogueElement>
    {
    }
}
