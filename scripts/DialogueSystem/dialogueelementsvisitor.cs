﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers;

namespace Doyl.ScriptDialogueSystem.Scripts.DialogueSystem
{
    public abstract class DialogueElementsVisitor
    {
        public virtual void Visit(DialogueLine dialogueLine)
        {
        
        }

        public virtual void Visit(DecisionSet decisionSet)
        {
        
        }

        public virtual void Visit(DialogueTriggerInvocation dialogueTriggerInvocation)
        {
        
        }

        public virtual void Visit(Jump jump)
        {
        
        }

        public virtual void Finish()
        {
        
        }
    }
}
