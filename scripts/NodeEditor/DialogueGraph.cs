﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using UnityEngine;
using XNode;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor
{
    [CreateAssetMenu(fileName = "NewDialogueGraph", menuName = "Script Dialogues/Dialogue Graph")]
    [System.Serializable]
    public class DialogueGraph : NodeGraph
    {
        [Header("Dialogue Info")]
        [SerializeField] public string title;
        [SerializeField] public List<string> actors; 
        [Header("Variables")]
        [SerializeField] public List<NodeEditorVariableDefinition> variables = new List<NodeEditorVariableDefinition>();
        [Header("Conditions")]
        [SerializeField] public List<NodeEditorConditionDefinition> conditions = new List<NodeEditorConditionDefinition>();
        [Header("Triggers")]
        [SerializeField] public List<NodeEditorTriggerDefinition> triggers = new List<NodeEditorTriggerDefinition>();
    }
}