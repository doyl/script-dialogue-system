﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Actors;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes;
using XNode;
//using NUnit.Framework;
using Debug = UnityEngine.Debug;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor
{
    public class DialogueNodeGraphConverter
    {
        private readonly DialogueGraph graph;
        public List<DialogueElement> elements;
        public List<Tag> tags;

        private readonly List<DialogueElementNode> processedNodes;
        private readonly Dictionary<DialogueElementNode, DialogueElement> jumpPlaces;
        private readonly Dictionary<DialogueElementNode, Jump> awaitingJumps;
    
        public DialogueNodeGraphConverter(DialogueGraph graph)
        {
            this.graph = graph;
            elements = new List<DialogueElement>();
            jumpPlaces = new Dictionary<DialogueElementNode, DialogueElement>();
            awaitingJumps = new Dictionary<DialogueElementNode, Jump>();
            processedNodes = new List<DialogueElementNode>();
            tags = new List<Tag>();
        }

        public Dialogue GetDialogue()
        {
            return new Dialogue(graph.title, GetActors(), tags, GetVariables(), GetConditions(), GetTriggers(), elements);
        }

        private List<Actor> GetActors()
        {
            return graph.actors.Select(x => new Actor(x)).ToList();
        }

        private List<DialogueVariable> GetVariables()
        {
            return graph.variables.Select(x => new DialogueVariable(x.variableName)).ToList();
        }

        private List<DialogueCondition> GetConditions()
        {
            return graph.conditions.Select(x => new DialogueCondition(x.conditionName)).ToList();
        }

        private List<DialogueTrigger> GetTriggers()
        {
            return graph.triggers.Select(x =>
                new DialogueTrigger(x.triggerName, x.argumentDefinitions.Select(t => t.variableType).ToArray())).ToList();
        }
    
        public Dialogue Convert()
        {
            SetupElements();
            return GetDialogue();
        }

        private void SetupElements()
        {
            DialogueBeginNode begin = (DialogueBeginNode)graph.nodes.Find(x => x is DialogueBeginNode);
            Visit(begin);
        }

        public void Visit(DialogueBeginNode beginNode)
        {
            NodePort nextPort = beginNode.GetOutputPort("dialogue").Connection;
            ProcessNextNode(nextPort);
        }
        public void Visit(ConditionNode conditionNode)
        {
            if (processedNodes.Contains(conditionNode))
            {
                elements.Add(AddJump(conditionNode));
                return;
            }
            processedNodes.Add(conditionNode);
            List<Argument> arguments = conditionNode.argumentsValues.ConvertAll<Argument>(x => new StringArgument(x)).ToList();
            ConditionUsage negatedCondition = new ConditionUsage(conditionNode.conditionName,true, arguments);
            NodePort nextPortPositive = conditionNode.GetOutputPort("nextOnFulfilled").Connection;
            NodePort nextPortNegative = conditionNode.GetOutputPort("nextOnFailed").Connection;
            
            DialogueElementNode negativeNode = nextPortNegative?.node as DialogueElementNode;
            if (negativeNode != null)
            {
                Jump jump = AddJump(negativeNode, negatedCondition);
                elements.Add(jump);
                ProcessNode(conditionNode, jump);
            }
            else
            {
                Jump jump = new Jump("End");
                elements.Add(jump);
                ProcessNode(conditionNode, jump);
            }
            ProcessNextNode(nextPortPositive);
            ProcessNextNode(nextPortNegative);
        }

        public void Visit(LineNode lineNode)
        {
            if (processedNodes.Contains(lineNode))
            {
                elements.Add(AddJump(lineNode));
                return;
            }
            DialogueLine line = new DialogueLine(lineNode.Id, new LineContent(lineNode.lineContent), lineNode.actor);
//            DialogueLine line = new DialogueLine(new LineContent(lineNode.lineContent), lineNode.actor, lineNode.id);
            elements.Add(line);
            ProcessNode(lineNode, line);
            NodePort nextPort = lineNode.GetOutputPort("next").Connection;
            ProcessNextNode(nextPort);
        }

        private void ProcessNextNode(NodePort connection)
        {
            if (connection != null)
            {
                DialogueElementNode nextNode = connection.node as DialogueElementNode;
                nextNode.Accept(this);
            }
            else
            {
                Jump endJump = new Jump("End");
                elements.Add(endJump);
            }
        }

        public void Visit(TriggerNode triggerNode)
        {
            if (processedNodes.Contains(triggerNode))
            {
                elements.Add(AddJump(triggerNode));
                return;
            }

            List<Argument> arguments = new List<Argument>();
            if (triggerNode.argumentsValues.Count != 0)
            {
                arguments =
                    triggerNode.argumentsValues.ConvertAll<Argument>(x => new StringArgument(x))
                        .ToList(); //Todo: Arguments should be of correct types
            }

            DialogueTriggerInvocation trigger = new DialogueTriggerInvocation(triggerNode.triggerName, arguments);
            elements.Add(trigger);
            ProcessNode(triggerNode, trigger);
            NodePort nextPort = triggerNode.GetOutputPort("next").Connection;
            ProcessNextNode(nextPort);
        }

        public void Visit(DecisionNode decisionNode)
        {
            if (processedNodes.Contains(decisionNode))
            {
                elements.Add(AddJump(decisionNode));
                return;
            }
            List<Decision> decisions = new List<Decision>();
            List<NodePort> portsToProcess = new List<NodePort>();
            for (int i = 0; i < decisionNode.decisions.Count(); ++i)
            {
                List<NodeEditorConditionUsage> conditionUsages = decisionNode.decisions[i].conditions;
                List<ConditionUsage> conditions = conditionUsages?.Select(x => new ConditionUsage(x.conditionName, false,
                    x.argumentsValues.Select(t => new StringArgument(t) as Argument).ToList())).ToList();
                Decision decision = new Decision(new LineContent(decisionNode.decisions[i].decisionText), "", conditions?.FirstOrDefault());
            
                NodePort decisionPort = decisionNode.GetPort("decisions " + i).Connection;
                if (decisionPort != null)
                {
                    portsToProcess.Add(decisionPort);
                    decision.jump = AddJump(decisionPort.node as DialogueElementNode);
                }
                else
                {
                    decision.DestinationTagName = "End";
                }
                decisions.Add(decision);
            }
            DecisionSet decisionSet = new DecisionSet(decisionNode.Id, decisions);
            elements.Add(decisionSet);
            ProcessNode(decisionNode, decisionSet);
            portsToProcess.ForEach(ProcessNextNode);
        }

        private Jump AddJump(DialogueElementNode destinationNode, ConditionUsage condition=null)
        {
            Jump jump = new Jump("");
            if (condition != null)
                jump.condition = condition;
            if (!jumpPlaces.Keys.Contains(destinationNode))
            {
                try
                {
                    awaitingJumps.Add(destinationNode, jump);
                }
                catch (Exception e)
                {
                    Debug.LogError(destinationNode);
                    throw e;
                }
            }
            else
            {
                DialogueElement jumpDestination = jumpPlaces[destinationNode];
                jump.DestinationIndex = elements.IndexOf(jumpDestination);
            }
            //elements.Add(jump);
            return jump;
        }

        private void ProcessNode(DialogueElementNode node, DialogueElement element)
        {
            processedNodes.Add(node);
            jumpPlaces.Add(node, element);
            if (awaitingJumps.Keys.Contains(node)) //Todo: Should work for many jumps to the same place
            {
                awaitingJumps[node].DestinationIndex = elements.IndexOf(element);
            }
        }
    }
}
