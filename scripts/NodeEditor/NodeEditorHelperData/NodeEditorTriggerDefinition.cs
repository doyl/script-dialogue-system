﻿using System.Collections.Generic;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData
{
    [System.Serializable]
    public class NodeEditorTriggerDefinition
    {
        [SerializeField] public string triggerName;
        [SerializeField] public List<NodeEditorArgumentDefinition> argumentDefinitions;
    }

    [System.Serializable]
    public class NodeEditorConditionDefinition
    {
        [SerializeField] public string conditionName;
        [SerializeField] public List<NodeEditorArgumentDefinition> argumentDefinitions;
    }

    [System.Serializable]
    public class NodeEditorVariableDefinition
    {
        [SerializeField] public string variableName;
        [SerializeField] public List<NodeEditorArgumentDefinition> argumentDefinitions;
    }


    [System.Serializable]
    public class NodeEditorArgumentDefinition
    {
        [SerializeField] public string name;
        [SerializeField] public ArgumentType variableType;
    }

    public enum ArgumentType
    {
        String,
        Int,
        Float
    };
}