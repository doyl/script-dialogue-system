﻿using System.Collections.Generic;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData
{
    [System.Serializable]
    public class NodeEditorDecision
    {
        public string decisionText;
        public List<NodeEditorConditionUsage> conditions;
    }

    [System.Serializable]
    public class NodeEditorConditionUsage
    {
        public string conditionName;
        public List<string> argumentsValues;
    }
}