﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Converter;
using UnityEngine;
using XNode;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes
{
	[NodeWidth(400)]
	[CreateNodeMenu("Dialogue Nodes/Line")]
	public class LineNode : DialogueElementNode
	{
		[Input] public NodeEditorDialogue previous;
		[Output] public NodeEditorDialogue next;
		[HideInInspector] public string actor;
		[HideInInspector] public string lineContent;
		public int SelectedIndex { get; set; }

		// Return the correct value of an output port when requested
		public override object GetValue(NodePort port) {
			return null; // Replace this
		}

		public override DialogueElement GetElement()
		{
			LineContent parsedLineContent = ScriptScriptOperation.ConvertLine(lineContent);
			return new DialogueLine(Id, parsedLineContent, actor);
		}

		public override void Accept(DialogueNodeGraphConverter converter)
		{
			converter.Visit(this);
		}
	}
}