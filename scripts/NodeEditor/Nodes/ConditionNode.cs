﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using JetBrains.Annotations;
using UnityEngine;
using XNode;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes
{
	[Serializable]
	[CreateNodeMenu("Dialogue Nodes/Condition")]
	public class ConditionNode : DialogueElementNode
	{
		[Input] [UsedImplicitly] 
		public NodeEditorDialogue previous;
		
		[Output] [UsedImplicitly] 
		public NodeEditorDialogue nextOnFulfilled;
		
		[Output] [UsedImplicitly] 
		public NodeEditorDialogue nextOnFailed;
		
		[HideInInspector] public string conditionName;
		[HideInInspector] public List<string> argumentsValues = new List<string>();
		[HideInInspector] public int selectedIndex;

		// Return the correct value of an output port when requested
		public override object GetValue(NodePort port) {
			return null; // Replace this
		}

		public override DialogueElement GetElement()
		{
			throw new NotImplementedException();
		}

		public override void Accept(DialogueNodeGraphConverter converter)
		{
			converter.Visit(this);
		}
	}
}