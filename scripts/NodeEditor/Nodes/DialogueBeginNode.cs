﻿using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using Node = XNode.Node;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes
{
    [CreateNodeMenu("Dialogue Nodes/Begin")]
    public class DialogueBeginNode : Node
    {
        [Output] public NodeEditorDialogue dialogue;
    }
}
