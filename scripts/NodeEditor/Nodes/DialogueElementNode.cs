﻿using System;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using UnityEngine;
using Node = XNode.Node;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes
{
    [Serializable]
    public abstract class DialogueElementNode : Node
    {
        [SerializeField]
        [HideInInspector]
        private string id;

        public Guid Id
        {
            get => string.IsNullOrEmpty(id) ? Guid.Empty : Guid.Parse(id);
            set => id = value.ToString();
        }

        public abstract DialogueElement GetElement();

        protected override void Init()
        {
            base.Init();
            if (Id == Guid.Empty)
            {
                Id = Guid.NewGuid();
                Debug.Log("Generated guid "+ id + " for graph " + graph.name);
            }

            base.Init();
        }

        public abstract void Accept(DialogueNodeGraphConverter converter);
    }
}
