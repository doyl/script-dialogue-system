﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes
{
    [System.Serializable]
    [CreateNodeMenu("Dialogue Nodes/Trigger")]
    public class TriggerNode : DialogueElementNode
    {
        [Input] public NodeEditorDialogue prev;
        [Output] public NodeEditorDialogue next;
        [HideInInspector] public string triggerName = "";
        [HideInInspector] public List<string> argumentsValues = new List<string>();
        [HideInInspector] public int selectedIndex;
    
        public override DialogueElement GetElement()
        {
            //new Argument
            //return new DialogueTriggerInvocation(triggerName, ); 
            return null;
        }

        public override void Accept(DialogueNodeGraphConverter converter)
        {
            converter.Visit(this);
        }
    }
}
