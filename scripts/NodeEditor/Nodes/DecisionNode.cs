﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.NodeEditor.NodeEditorHelperData;
using UnityEngine;
using XNode;

namespace Doyl.ScriptDialogueSystem.Scripts.NodeEditor.Nodes
{
	[NodeWidth(400)]
	[CreateNodeMenu("Dialogue Nodes/Decision Block")]
	public class DecisionNode : DialogueElementNode
	{
		[Input] [SerializeField] private NodeEditorDialogue previous;
		[Output(dynamicPortList = true)] [SerializeField] public List<NodeEditorDecision> decisions;

		// Return the correct value of an output port when requested
		public override object GetValue(NodePort port) {
			return null; // Replace this
		}

		public override DialogueElement GetElement()
		{
			throw new System.NotImplementedException();
		}

		public override void Accept(DialogueNodeGraphConverter converter)
		{
			converter.Visit(this);
		}
	}
}