﻿using System;
using System.Collections.Generic;

namespace Doyl.ScriptDialogueSystem.Scripts.LocalizationSystem
{
    public interface ILocalizable
    {
        List<Tuple<string, string>> GetTranslations();
    }
}
