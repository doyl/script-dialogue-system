﻿using System.Collections.Generic;

namespace Doyl.ScriptDialogueSystem.Scripts.LocalizationSystem
{
    public class LocalizationDictionary
    {
        public string LanguageCode { get; }
        public Dictionary<string, string> Translations { get; set; }

        public LocalizationDictionary(string languageCode, Dictionary<string, string> translations)
        {
            LanguageCode = languageCode;
            Translations = translations;
        }
    }
}