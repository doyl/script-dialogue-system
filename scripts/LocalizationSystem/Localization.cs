﻿using System.Collections.Generic;

namespace Doyl.ScriptDialogueSystem.Scripts.LocalizationSystem
{
    public class Localization
    {
        private readonly CsvLoader csvLoader;
        private List<LocalizationDictionary> localizationDictionaries;

        public Localization(CsvLoader csvLoader)
        {
            this.csvLoader = csvLoader;
            localizationDictionaries = new List<LocalizationDictionary>();
        }

        public bool isInit;

        public void Init()
        {
            csvLoader.LoadCsv();
            localizationDictionaries.Add(new LocalizationDictionary("En", new Dictionary<string, string>()));
            UpdateDictionaries();
            isInit = true;
        }

        public void UpdateDictionaries()
        {
            localizationDictionaries.ForEach(localizationDictionary =>
            {
                localizationDictionary.Translations =
                    csvLoader.GetDictionaryValues(localizationDictionary.LanguageCode);
            });
        }
    
        public LocalizationDictionary GetDictionaryValues(string languageCode)
        {
            if(!isInit)
                Init();
        
            return localizationDictionaries.Find(dictionary => dictionary.LanguageCode.Equals(languageCode));
        }

        public string GetLocalizedValue(string key, string languageCode)
        {
            string value = key;
            GetDictionaryValues(languageCode).Translations.TryGetValue(key, out value);
            return value;
        }

        public void Add(string key, string value, string languageCode)
        {
            if (value.Contains("\""))
            {
                value = value.Replace('"', '\"');
            }
        
            csvLoader.LoadCsv();
            csvLoader.Add(key, value);
            csvLoader.LoadCsv();
        
            UpdateDictionaries();
        }

        public void Replace(string key, string value, string languageCode)
        {
            if (value.Contains("\""))
            {
                value.Replace('"', '\"');
            }
        
            csvLoader.LoadCsv();
            csvLoader.Edit(key, value);
            csvLoader.LoadCsv();
        }

        public void RemoveRecord(string key)
        {
            csvLoader.LoadCsv();
            csvLoader.Remove(key);
            csvLoader.LoadCsv();
            
            UpdateDictionaries();
        }

        public void RemoveLocalizationDictionary(string languageCode)
        {
            localizationDictionaries.RemoveAll(localizationDictionary =>
                localizationDictionary.LanguageCode.Equals(languageCode));
        }
    }
}
