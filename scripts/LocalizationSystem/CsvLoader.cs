﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.LocalizationSystem
{
    public class CsvLoader
    {
        private TextAsset csvFile;
        private char lineSeparator = '\n';
        private char surround = '"';
        private string[] fieldSeparator = {"\",\""};

        public void LoadCsv(string path = "localisation")
        {
            csvFile = Resources.Load<TextAsset>(path);
        }
        public Dictionary<string, string> GetDictionaryValues(string attributeId)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            string[] lines = csvFile.text.Split(lineSeparator);

            int attributeIndex = -1;

            string[] headers = lines[0].Split(fieldSeparator, StringSplitOptions.None);

            for (int i = 0; i < headers.Length; ++i)
            {
                if (headers[i].Contains(attributeId))
                {
                    attributeIndex = i;
                    break;
                }
            }
            Regex CsvParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            for (int i = 0; i < lines.Length; ++i)
            {
                string line = lines[i];

                string[] fields = CsvParser.Split(line);

                for (int j = 0; j < fields.Length; ++j)
                {
                    fields[j] = fields[j].TrimStart(' ', surround);
                    fields[j] = fields[j].TrimEnd(surround);
                }

                if (fields.Length > attributeIndex)
                {
                    var key = fields[0];

                    if (dictionary.ContainsKey(key))
                    {
                        continue;
                    }

                    var value = fields[attributeIndex];
                
                    dictionary.Add(key, value);
                }
            }

            return dictionary;
        }

        public void Add(string key, string value)
        {
#if UNITY_EDITOR
            string appended = String.Format("\n\"{0}\",\"{1}\",\"\"", key, value.TrimEnd('\r'));
            File.AppendAllText("Assets/Resources/localisation.csv", appended);
        
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public void Remove(string key)
        {
            string[] lines = csvFile.text.Split(lineSeparator);
        
            string[] keys = new string[lines.Length];

            for (int i = 0; i < lines.Length; ++i)
            {
                string line = lines[i];
                keys[i] = line.Split(fieldSeparator, StringSplitOptions.None)[0];
            }

            int index = -1;

            for (int i = 0; i < keys.Length; ++i)
            {
                if (keys[i].Contains(key))
                {
                    index = i;
                    break;
                }
            }

            if (index > -1)
            {
                string[] newLines;
                newLines = lines.Where(t => t != lines[index]).ToArray();

                string replaced = string.Join(lineSeparator.ToString(), newLines);
                File.WriteAllText("Assets/Resources/localisation.csv", replaced);
            }
        }

        public void Edit(string key, string value)
        {
            Remove(key);
            Add(key, value);
        }
    }
}
