﻿using System;
using System.Collections.Generic;
using System.Text;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer
{
    public class ScriptScriptTokenizer : IScriptScriptTokenizer
    {
        public string ResultText => resultText.ToString();
        private StringBuilder resultText  = new StringBuilder();
        private List<Token> tokensDefinitions;
        private string remainingText;
        private string backupText;
        public int LineCounter { get; private set; }
        
        public ScriptScriptTokenizer()
        {
            FillTokenDefinitions();
        }

        public void LoadScript(string text)
        {
            LineCounter = 1;
            remainingText = text;
        }

        private void FillTokenDefinitions()
        {
            tokensDefinitions = new List<Token>
            {
                new Token(TokenType.Number, "^\\d+"),
                new Token(TokenType.OpenTrigger, "^\\("),
                new Token(TokenType.CloseTrigger, "^\\)"),
                new Token(TokenType.TitleKeyword, "^Title"),
                new Token(TokenType.ActorsKeyword, "^Actors"),
                new Token(TokenType.VariablesKeyword, "^Variables"),
                new Token(TokenType.ConditionsKeyword, "^Conditions"),
                new Token(TokenType.TriggersKeyword, "^Triggers"),
                new Token(TokenType.BeginKeyword, "^Begin"),
                new Token(TokenType.EndKeyword, "^End"),
                new Token(TokenType.Many, "^many"),
                new Token(TokenType.IntType, "^int"),
                new Token(TokenType.StringType, "^string"),
                new Token(TokenType.FloatType, "^float"),
                new Token(TokenType.Comma, "^\\,"),
                new Token(TokenType.Colon, "^\\:"),
                new Token(TokenType.Period, "^\\."),
                new Token(TokenType.Jump, "^>>"),
                new Token(TokenType.Tag, "^\\[[a-z|0-9]*\\]"),
                new Token(TokenType.BooleanOperator, "^([||] | [&&])"),
                new Token(TokenType.NegateOperator, "^!"),
                new Token(TokenType.OpenCondition, "^\\<"),
                new Token(TokenType.CloseCondition, "^\\>"),
                new Token(TokenType.OpenVariable, "^\\{"),
                new Token(TokenType.CloseVariable, "^\\}"),
                new Token(TokenType.StringValue, "^\"[^\"\n]*\""),
                new Token(TokenType.Word, "^([AaĄąBbCcĆćDdEeĘęFfGgHhIiJjKkLlŁłMmNnŃńOoÓóPpRrSsŚśTtUuVvWwXxYyZzŹźŻż]|[0-9]|\")+"),
                new Token(TokenType.Return, "^\n"),
                new Token(TokenType.Id, "^\\@(\\{){0,1}[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}(\\}){0,1}")
            };
            //tokensDefinitions.Add(new Token(TokenType.CompareOperator, "^(== | != | <= | >= | > | <)"));
        }

        public DslToken NextToken()
        {
            DslToken token;
            backupText = remainingText;
            while (!string.IsNullOrWhiteSpace(remainingText))
            {
                if (remainingText[0] == '\n')
                    ++LineCounter;
                
                TokenMatch match = FindMatch(remainingText);
                if (match.IsMatch)
                {
                    token = new DslToken(match.Type, match.Value);
                    remainingText = match.RemainingText;
                    return token;
                }

                if (char.IsWhiteSpace(remainingText[0]))
                {
                    resultText.Append(remainingText[0]);
                    remainingText = remainingText.Substring(1);
                }
                else
                {
                    throw new ScriptScriptException("Invalid character at line " + LineCounter +" in given script: " + remainingText[0]);
                }
            }

            return null;
        }

        public void UndoNextToken()
        {
            if (backupText != null)
            {
                remainingText = backupText;
            }
        }

        public void SaveToken(DslToken token)
        {
            resultText.Append(token.Value);
        }

        public string GetLine(params char[] until)
        {
            int indexOfEnd = remainingText.Length;
            foreach (char c in until)
            {
                if (remainingText.Contains(c.ToString()))
                {
                    indexOfEnd = Mathf.Min(indexOfEnd, remainingText.IndexOf(c));
                }
            }
            string lineContent = remainingText.Substring(0, indexOfEnd);
            remainingText = remainingText.Substring(indexOfEnd);
            resultText.Append(lineContent);
            return lineContent;
        }

        public DslToken ProvideId()
        {
            Guid generatedGuid = Guid.NewGuid();
            string newId = "@" + generatedGuid;
            TokenMatch match = FindMatch(newId);
            if (match.Type == TokenType.Id)
            {
                resultText.Append(newId).Append("\n");
                ++LineCounter;
                return new DslToken(match.Type, match.Value);
            }
            throw new ScriptScriptException("Error while generating ID for element. " + newId + " does not match id pattern.");
        }

        public void RemoveIdLine()
        {
            StringBuilder resultString = new StringBuilder();
            string inputString = resultText.ToString();
            int idBeginning = inputString.LastIndexOf('@');
            int idEnd = inputString.Substring(idBeginning).IndexOf('\n') + idBeginning;
            resultString.Append(resultText.ToString(0, idBeginning - 1));
            int length = resultText.ToString().Length;
            if(resultText.ToString().Length - 1 > idEnd + 1)
                resultString.Append(resultText.ToString(idEnd, resultText.ToString().Length - idEnd - 1));
            resultText = resultString;
        }

        private TokenMatch FindMatch(string lqlText)
        {
            foreach (var tokenDefinition in tokensDefinitions)
            {
                var match = tokenDefinition.Match(lqlText);
                if (match.IsMatch)
                    return match;
            }

            return new TokenMatch() { IsMatch = false };
        }
    }
}