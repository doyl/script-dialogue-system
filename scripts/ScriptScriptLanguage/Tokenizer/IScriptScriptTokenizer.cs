﻿namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer
{
    public interface IScriptScriptTokenizer
    {
        void LoadScript(string text);
        DslToken NextToken();
        void UndoNextToken();
        string GetLine(char[] until);
        void SaveToken(DslToken token);
        int LineCounter { get; }
        string ResultText { get; }
        DslToken ProvideId();
        void RemoveIdLine();
    }
}
