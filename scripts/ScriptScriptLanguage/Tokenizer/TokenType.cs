﻿namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer
{
    public enum TokenType
    {
        Word, Number, StringValue,
        OpenTrigger, CloseTrigger, OpenCondition, CloseCondition, OpenVariable, CloseVariable,
        TitleKeyword, ActorsKeyword, VariablesKeyword, ConditionsKeyword, TriggersKeyword, BeginKeyword, EndKeyword,
        Many, StringType, IntType, FloatType,
        Comma, Colon, Period, Jump, Tag,
        CompareOperator, BooleanOperator, NegateOperator,
        Return,
        Id,
        OpenId
    };
}
