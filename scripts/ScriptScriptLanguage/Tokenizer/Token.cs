﻿using System.Text.RegularExpressions;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer
{
    public class Token
    {
        private readonly Regex regex;
        private readonly TokenType type;

        public Token(TokenType type, string regexPattern)
        {
            this.type = type;
            regex = new Regex(regexPattern, RegexOptions.IgnoreCase);
        }

        public TokenMatch Match(string inputString)
        {
            var match = regex.Match(inputString);
            if(match.Success)
            {
                string remainingText = string.Empty;
                if (match.Length != inputString.Length)
                    remainingText = inputString.Substring(match.Length);

                return new TokenMatch
                {
                    IsMatch = true,
                    RemainingText = remainingText,
                    Type = type,
                    Value = match.Value
                };
            }
            return new TokenMatch { IsMatch = false };
        }
    }

    public class TokenMatch
    {
        public bool IsMatch { get; set; }
        public TokenType Type { get; set; }
        public string Value { get; set; }
        public string RemainingText { get; set; }
    }

    public class DslToken
    {
        public TokenType Type { get; set; }
        public string Value { get; set; }

        public DslToken(TokenType type, string value)
        {
            Type = type;
            Value = value;
        }
    }
}