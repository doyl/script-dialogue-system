﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Converter
{
    public class ScriptScriptConverter
    {
        private readonly IScriptScriptTokenizer scriptScriptTokenizer;
        private readonly ScriptScriptParser scriptScriptParser;
        private readonly DialogueMapperVisitor dialogueMapperVisitor;

        public ScriptScriptConverter(IScriptScriptTokenizer scriptScriptTokenizer,
            ScriptScriptParser scriptScriptParser,
            DialogueMapperVisitor dialogueMapperVisitor)
        {
            this.scriptScriptTokenizer = scriptScriptTokenizer;
            this.scriptScriptParser = scriptScriptParser;
            this.dialogueMapperVisitor = dialogueMapperVisitor;
        }
    
        public Dialogue Convert(string scriptText)
        {
            scriptScriptTokenizer.LoadScript(scriptText);
            ScriptScriptDialogueRoot scriptScriptDialogueRoot = scriptScriptParser.ParseDialogueScript(out string resultText);
            Debug.Log(resultText); //TODO: Update script
            return dialogueMapperVisitor.MapToDialogue(scriptScriptDialogueRoot);
        }
    }
}
