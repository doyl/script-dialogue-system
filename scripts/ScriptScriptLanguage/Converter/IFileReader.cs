﻿namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Converter
{
    public interface IFileReader
    {
        void LoadFile(string path);
        char GetNextCharacter();
        string GetNextLine();
        string GetWholeText();
    }
}
