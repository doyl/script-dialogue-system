﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.NodeParsers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Converter
{
    public static class ScriptScriptOperation
    {
        public static IScriptScriptTokenizer scriptScriptTokenizer {get; private set;}
        public static ScriptScriptTokenizerProxy scriptScriptTokenizerProxy {get; private set;}
        public static DialogueIdParser dialogueIdParser {get; private set;}
        public static ArgumentsParser argumentsParser {get; private set;}
        public static VariableParser variableParser {get; private set;}
        public static LineContentParser lineContentParser {get; private set;}
        public static LineParser lineParser {get; private set;}
        public static JumpParser jumpParser {get; private set;}
        public static DecisionsParser decisionsParser {get; private set;}
        public static DialogueParser dialogueParser {get; private set;}
        public static DialogueElementParser dialogueElementParser {get; private set;}
        public static DialogueBlockParser dialogueBlockParser {get; private set;}
        public static TriggerParser triggerParser {get; private set;}
        public static ConditionParser conditionParser {get; private set;}
        public static BeginningParser beginningParser {get; private set;}
        public static ScriptScriptParser scriptScriptParser {get; private set;}
        public static DialogueMapperVisitor dialogueMapperVisitor {get; private set;}
        public static ScriptScriptConverter scriptScriptConverter {get; private set;}

        public static Dialogue Convert(string text)
        {
            SetupTokenizersAndParsers();
            return scriptScriptConverter.Convert(text);
        }

        public static LineContent ConvertLine(string line)
        {
            SetupTokenizersAndParsers();
            scriptScriptTokenizer.LoadScript(line);
            return lineContentParser.ParseLineContent();
        }

        private static void SetupTokenizersAndParsers()
        {
            scriptScriptTokenizer = new ScriptScriptTokenizer();
            scriptScriptTokenizerProxy = new ScriptScriptTokenizerProxy(scriptScriptTokenizer);
            argumentsParser = new ArgumentsParser(scriptScriptTokenizerProxy);
            variableParser = new VariableParser(scriptScriptTokenizerProxy, argumentsParser);
            lineContentParser = new LineContentParser(scriptScriptTokenizerProxy, variableParser);
            lineParser = new LineParser(scriptScriptTokenizerProxy, lineContentParser);
            triggerParser = new TriggerParser(scriptScriptTokenizerProxy, argumentsParser);
            conditionParser = new ConditionParser(scriptScriptTokenizerProxy, argumentsParser);
            jumpParser = new JumpParser(scriptScriptTokenizerProxy);
            decisionsParser = new DecisionsParser(scriptScriptTokenizerProxy, conditionParser, jumpParser, lineContentParser);
            dialogueElementParser = new DialogueElementParser(scriptScriptTokenizerProxy, lineParser, jumpParser, decisionsParser, triggerParser);
            dialogueIdParser = new DialogueIdParser(scriptScriptTokenizerProxy);
            dialogueBlockParser = new DialogueBlockParser(scriptScriptTokenizerProxy, dialogueIdParser, conditionParser, dialogueElementParser);
            beginningParser = new BeginningParser(scriptScriptTokenizerProxy);
            dialogueParser = new DialogueParser(scriptScriptTokenizerProxy, dialogueBlockParser);
            scriptScriptParser = new ScriptScriptParser(scriptScriptTokenizerProxy, beginningParser, dialogueParser, dialogueIdParser);
            dialogueMapperVisitor = new DialogueMapperVisitor();
            scriptScriptConverter = new ScriptScriptConverter(scriptScriptTokenizer, scriptScriptParser, dialogueMapperVisitor);
        }
    }
}