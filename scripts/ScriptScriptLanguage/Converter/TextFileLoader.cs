﻿using System.IO;
using UnityEngine;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Converter
{
    public class TextFileLoader
    {
        public string LoadTextFromFile(string filePath)
        {
            try
            {   
                using (StreamReader sr = new StreamReader(filePath))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (IOException ex)
            {
                Debug.LogError("The file could not be read: " + ex.Message);
                return null;
            }
        }
    }
}
