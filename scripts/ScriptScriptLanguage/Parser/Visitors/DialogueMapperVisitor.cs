﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Actors;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Variables;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors
{
    public class DialogueMapperVisitor : IParseTreeVisitor
    {
        private Guid id;
        string title;
        readonly Dictionary<string, Actor> actors = new Dictionary<string, Actor>();
        readonly Dictionary<string, DialogueVariable> variables = new Dictionary<string, DialogueVariable>();
        readonly Dictionary<string, DialogueTrigger> triggers = new Dictionary<string, DialogueTrigger>();
        readonly Dictionary<string, DialogueCondition> conditions = new Dictionary<string, DialogueCondition>();
        readonly Dictionary<string, Tag> tags = new Dictionary<string, Tag>();
        readonly List<DialogueElement> elements = new List<DialogueElement>();

        readonly List<Decision> awaitingDecisions = new List<Decision>();
        private ConditionUsage awaitingCondition;
        
        public Dialogue MapToDialogue(ScriptScriptDialogueRoot scriptScriptDialogueRoot)
        {
            scriptScriptDialogueRoot.Accept(this);
            BindJumpsWithTags();
            return GetDialogue();
        }

        public void Visit(ScriptScriptDialogueRoot scriptScriptDialogueRoot)
        {
            id = Guid.Parse(scriptScriptDialogueRoot.Id);
        }
        
        public void Visit(ConditionDefinitionNode conditionDefinitionNode)
        {
            if(!conditions.Keys.Contains(conditionDefinitionNode.Name)) //TODO: Use signature using arguments to allow same name for conditions with different arguments number 
                conditions.Add(conditionDefinitionNode.Name, new DialogueCondition(conditionDefinitionNode.Name)); //TODO: Check arguments 
        }

        public void Visit(ConditionExpressionNode conditionExpressionNode)
        {
            ConditionUsageNode conditionUsageNode = conditionExpressionNode.Conditions[0][0]; //TODO: Use full expression
            ConditionUsage newCond = new ConditionUsage(conditionUsageNode.ConditionName, conditionUsageNode.IsNegated, conditionUsageNode.ArgumentsNode.GetArguments());
            if (!conditions.Keys.Contains(newCond.Name))
            {
                throw new ScriptScriptException("Condition " + newCond.Name + " used but not defined!");
            }
            awaitingCondition = newCond;
        }

        public void Visit(DecisionNode decisionNode)
        {
            LineContent decisionContent = decisionNode.Content;
            string tagNameDestination = decisionNode.Jump.Tag;
            //Debug.Log("Decision met with condition "+ awaitingCondition.Name);
            awaitingDecisions.Add(new Decision(decisionContent, tagNameDestination, awaitingCondition));
            awaitingCondition = null;
        }

        public void Visit(DecisionSetNode decisionSetNode)
        {
            decisionSetNode.CreatedDecisions.AddRange(awaitingDecisions);
            awaitingDecisions.Clear();
        }

        public void Visit(DialogueBlockNode dialogueBlockNode)
        {
            DialogueElement element = dialogueBlockNode.DialogueElement.GetElement();
            if (dialogueBlockNode.Tag != "")
                tags[dialogueBlockNode.Tag] = new Tag(dialogueBlockNode.Tag, element);
            if (dialogueBlockNode.Conditions != null)
            {
                element.condition = awaitingCondition;
                awaitingCondition = null;
            }

            elements.Add(element);
        }

        public void Visit(TitlesDeifnitionsNode titlesDefinitionsNode)
        {
            title = titlesDefinitionsNode.Title;
        }

        public void Visit(TriggerDefinitionNode triggerDefinitionNode)
        {
            triggers.Add(triggerDefinitionNode.Name, new DialogueTrigger(triggerDefinitionNode.Name));
        }

        public void Visit(TriggerNode triggerNode)
        {
            if (!triggers.Keys.Contains(triggerNode.TriggerName))
            {
                throw new ScriptScriptException("Trigger " + triggerNode.TriggerName + " used but not defined!");
            }
        }

        public void Visit(VariableDefinitionNode variableDefinition)
        {
            variables[variableDefinition.VariableName] = new DialogueVariable(variableDefinition.VariableName);
        }
        
        private Dialogue GetDialogue()
        {
            return new Dialogue(id, title, actors, tags, variables, conditions, triggers, elements);
        }

        private void BindJumpsWithTags()
        {
            foreach (DialogueElement element in elements)
            {
                if (element is Jump jump)
                {
                    if (jump.DestinationTagName == "End") continue;
                    try
                    {
                        Tag destinationTag = tags[jump.DestinationTagName];
                        jump.DestinationIndex = elements.IndexOf(destinationTag.DialogueElement);
                    }
                    catch (Exception)
                    {
                        throw new ScriptScriptException("Jump destination does not exist: " + jump.DestinationTagName);
                    }
                }
                if (element is DecisionSet decisions)
                {
                    foreach (Decision decision in decisions.Decisions)
                    {
                        if (decision.DestinationTagName == "End") continue;
                        try
                        {
                            Tag destinationTag = tags[decision.DestinationTagName];
                            decision.DestinationIndex = elements.IndexOf(destinationTag.DialogueElement);
                        }
                        catch (Exception)
                        {
                            throw new ScriptScriptException("Jump destination does not exist: " + decision.DestinationTagName);
                        }
                    }
                }
            }
        }
    }
}
