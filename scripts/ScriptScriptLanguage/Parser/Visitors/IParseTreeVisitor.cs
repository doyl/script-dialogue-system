﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Variables;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors
{
    public interface IParseTreeVisitor
    {
        void Visit(ScriptScriptDialogueRoot scriptScriptDialogueRoot);
        void Visit(ConditionDefinitionNode conditionDefinitionNode);
        void Visit(ConditionExpressionNode conditionExpressionNode);
        void Visit(DecisionNode decisionNode);
        void Visit(DecisionSetNode decisionSetNode);
        void Visit(DialogueBlockNode dialogueBlockNode);
        void Visit(TitlesDeifnitionsNode titlesDefinitionsNode);
        void Visit(TriggerDefinitionNode triggerDefinitionNode);
        void Visit(TriggerNode triggerNode);
        void Visit(VariableDefinitionNode argumentsDeclarationsNode);
    }
}
