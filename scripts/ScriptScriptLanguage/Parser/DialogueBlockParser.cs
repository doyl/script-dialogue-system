﻿using System;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class DialogueBlockParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly DialogueIdParser dialogueIdParser;
        private readonly ConditionParser conditionParser;
        private readonly DialogueElementParser dialogueElementParser;

        public DialogueBlockParser(ScriptScriptTokenizerProxy tokenizerProxy, DialogueIdParser dialogueIdParser, ConditionParser conditionParser, DialogueElementParser dialogueElementParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.dialogueIdParser = dialogueIdParser;
            this.conditionParser = conditionParser;
            this.dialogueElementParser = dialogueElementParser;
        }

        public DialogueBlockNode ParseDialogueBlock()
        {
            string tag = "";
            DslToken tagToken;
            if ((tagToken = tokenizerProxy.Accept(TokenType.Tag)) != null)
            {
                tag = tagToken.Value;
                tokenizerProxy.Expect(TokenType.Return);
            }

            string id = dialogueIdParser.ParseId();

            ConditionExpressionNode conditions = null;
            if (tokenizerProxy.Check(TokenType.OpenCondition) || tokenizerProxy.Check(TokenType.NegateOperator))
            {
                conditions = conditionParser.ParseConditionsExpression();
                tokenizerProxy.Expect(TokenType.Return);
            }

            DialogueElementNode dialogueElement = dialogueElementParser.ParseDialogueElement();
            dialogueElement.id = Guid.Parse(id);
            return new DialogueBlockNode(dialogueElement, tag, id, conditions);
        }
    }
}