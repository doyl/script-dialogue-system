﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.NodeParsers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class DecisionsParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly ConditionParser conditionParser;
        private readonly JumpParser jumpParser;
        private readonly LineContentParser lineContentParser;

        public DecisionsParser(ScriptScriptTokenizerProxy tokenizerProxy, ConditionParser conditionParser, JumpParser jumpParser, LineContentParser lineContentParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.conditionParser = conditionParser;
            this.jumpParser = jumpParser;
            this.lineContentParser = lineContentParser;
        }
        
        public DecisionSetNode ParseDecisionBlock()
        {
            List<DecisionNode> decisions = new List<DecisionNode>();
            while (tokenizerProxy.Check(TokenType.Number))
            {
                decisions.Add(ParseDecision());
            }
            return new DecisionSetNode(decisions);
        }

        private DecisionNode ParseDecision()
        {
            ConditionExpressionNode conditions = null;
            tokenizerProxy.Expect(TokenType.Number);
            tokenizerProxy.Expect(TokenType.Period);
            if (tokenizerProxy.Check(TokenType.OpenCondition) || tokenizerProxy.Check(TokenType.NegateOperator))
            {
                conditions = conditionParser.ParseConditionsExpression();
            }
            LineContent content = lineContentParser.ParseLineContent('>');
            JumpNode jump = jumpParser.ParseJump();
            return new DecisionNode(content, jump, conditions);
        }
    }
}