﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.NodeParsers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class DialogueElementParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly ConditionParser conditionParser;
        private readonly LineParser lineParser;
        private readonly DecisionsParser decisionsParser;
        private readonly JumpParser jumpParser;
        private readonly TriggerParser triggerParser;

        public DialogueElementParser(ScriptScriptTokenizerProxy tokenizerProxy, LineParser lineParser, JumpParser jumpParser, DecisionsParser decisionsParser, TriggerParser triggerParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.lineParser = lineParser;
            this.jumpParser = jumpParser;
            this.decisionsParser = decisionsParser;
            this.triggerParser = triggerParser;
        }

        public DialogueElementNode ParseDialogueElement()
        {
            DialogueElementNode dialogueElement;
            bool needsId;
            if (tokenizerProxy.Check(TokenType.Word))
            {
                needsId = true;
                dialogueElement = lineParser.ParseLine();
            }
            else if (tokenizerProxy.Check(TokenType.Jump))
            {
                needsId = false;
                dialogueElement = jumpParser.ParseJump();
            }
            else if (tokenizerProxy.Check(TokenType.Number))
            {
                needsId = true;
                dialogueElement = decisionsParser.ParseDecisionBlock();
            }
            else
            {
                needsId = false;
                dialogueElement = triggerParser.ParseTrigger();
            }

            if (!needsId)
            {
                tokenizerProxy.RemoveIdLine();
            }
            tokenizerProxy.Expect(TokenType.Return);
            return dialogueElement;
        }
    }
}