﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class ScriptScriptTokenizerProxy
    {
        private readonly IScriptScriptTokenizer tokenizer;
        private DslToken currentToken;

        public ScriptScriptTokenizerProxy(IScriptScriptTokenizer scriptScriptTokenizer)
        {
            tokenizer = scriptScriptTokenizer;
        }

        public void NextToken()
        {
            currentToken = tokenizer.NextToken();
        }

        public bool Check(TokenType token)
        {
            return currentToken?.Type == token;
        }

        public string AcceptLine(params char[] until)
        {
            tokenizer.UndoNextToken();
            string lineContent = tokenizer.GetLine(until);
            NextToken();
            return lineContent;
        }
        
        public DslToken Accept(TokenType tokenType)
        {
            if (currentToken.Type == tokenType)
            {
                DslToken tempToken = currentToken;
                tokenizer.SaveToken(tempToken);
                NextToken();
                return tempToken;
            }
            return null;
        }

        public DslToken Accept(List<TokenType> tokenTypes)
        {
            foreach (TokenType type in tokenTypes)
            {
                if (currentToken.Type == type)
                {
                    DslToken tempToken = currentToken;
                    NextToken();
                    return tempToken;
                }
            }
            return null;
        }

        public DslToken Accept(params TokenType[] tokenTypes)
        {
            foreach (TokenType type in tokenTypes)
            {
                if (currentToken.Type == type)
                {
                    DslToken tempToken = currentToken;
                    NextToken();
                    return tempToken;
                }
            }
            return null;
        }

        public DslToken Expect(TokenType tokenType)
        {
            TokenType type = currentToken.Type;
            DslToken token = Accept(tokenType);
            if (token != null)
            {
                return token;
            }
            throw new ScriptScriptException
                ("Error at line " + tokenizer.LineCounter + ". " + type + " not expected. Instead try: " + tokenType);
        }

        public string GetResultText()
        {
            return tokenizer.ResultText;
        }

        public DslToken ProvideId()
        {
            return tokenizer.ProvideId();
        }

        public void RemoveIdLine()
        {
            tokenizer.RemoveIdLine();
        }
    }
}