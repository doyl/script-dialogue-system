﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class JumpParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;

        public JumpParser(ScriptScriptTokenizerProxy tokenizerProxy)
        {
            this.tokenizerProxy = tokenizerProxy;
        } 
        
        public JumpNode ParseJump()
        {
            JumpNode jump;
            tokenizerProxy.Expect(TokenType.Jump);
            string tag;
            if (tokenizerProxy.Accept(TokenType.EndKeyword) == null)
            {
                tag = tokenizerProxy.Expect(TokenType.Tag).Value;
                jump = new JumpNode(tag);
            }
            else
            {
                jump = new EndJumpNode();
            }
            tokenizerProxy.Expect(TokenType.Return);
            return jump;
        }
    }
}