﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class TriggerParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly ArgumentsParser argumentsParser;

        public TriggerParser(ScriptScriptTokenizerProxy tokenizerProxy, ArgumentsParser argumentsParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.argumentsParser = argumentsParser;
        }
        
        public TriggerNode ParseTrigger()
        {
            tokenizerProxy.Expect(TokenType.OpenTrigger);
            string name = tokenizerProxy.Expect(TokenType.Word).Value;
            ArgumentsNode arguments = ArgumentsNode.Empty();
            if (tokenizerProxy.Accept(TokenType.Colon) != null)
            {
                arguments = argumentsParser.Parse();
            }
            tokenizerProxy.Expect(TokenType.CloseTrigger);
            tokenizerProxy.Expect(TokenType.Return);
            return new TriggerNode(name, arguments);
        }
    }
}