﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Actors;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Variables;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class BeginningParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;

        public BeginningParser(ScriptScriptTokenizerProxy tokenizerProxy)
        {
            this.tokenizerProxy = tokenizerProxy;
        }
        public BeginningNode Parse()
        {
            TitlesDeifnitionsNode titlesDefinitions = ParseTitleDefinition();
            ActorsDefinitionsNode actorsDef = ParseActorsDefinitions();
            VariablesDefinitionsNode variablesDef = ParseVariablesDefinitions();
            ConditionsDefinitionsNode conditionsDef = ParseConditionsDefinitions();
            TriggersDefinitionsNode triggersDef = ParseTriggersDefinitions();
            tokenizerProxy.Expect(TokenType.Return);
            ParseBeginKeyword();
            tokenizerProxy.Expect(TokenType.Return);
            tokenizerProxy.Expect(TokenType.Return);
            return new BeginningNode(titlesDefinitions, actorsDef, variablesDef, conditionsDef, triggersDef);
        }
        
        private TitlesDeifnitionsNode ParseTitleDefinition()
        {
            string title;
            tokenizerProxy.Expect(TokenType.TitleKeyword);
            tokenizerProxy.Expect(TokenType.Colon);
            title = ParseLineOfText();
            //Debug.Log("Title is: " + title);
            tokenizerProxy.Expect(TokenType.Return);
            return new TitlesDeifnitionsNode(title);
        }
        private ActorsDefinitionsNode ParseActorsDefinitions()
        {
            List<ActorNode> actors = new List<ActorNode>();
            tokenizerProxy.Expect(TokenType.ActorsKeyword);
            tokenizerProxy.Expect(TokenType.Colon);
            string name = tokenizerProxy.Expect(TokenType.Word).Value;
            actors.Add(new ActorNode(name));
            while (tokenizerProxy.Accept(TokenType.Comma) != null)
            {
                name = tokenizerProxy.Expect(TokenType.Word).Value;
                actors.Add(new ActorNode(name));
            }
            tokenizerProxy.Expect(TokenType.Return);
            return new ActorsDefinitionsNode(actors);
        }

        private VariablesDefinitionsNode ParseVariablesDefinitions()
        {
            List<VariableDefinitionNode> variables = new List<VariableDefinitionNode>();
            if (tokenizerProxy.Accept(TokenType.VariablesKeyword) != null)
            {
                tokenizerProxy.Expect(TokenType.Colon);
                variables.Add(ParseSingleVariableDefinition());
                while (tokenizerProxy.Accept(TokenType.Comma) != null)
                {
                    variables.Add(ParseSingleVariableDefinition());
                }

                tokenizerProxy.Expect(TokenType.Return);
            }
            return new VariablesDefinitionsNode(variables);
        }

        private VariableDefinitionNode ParseSingleVariableDefinition()
        {
            tokenizerProxy.Expect(TokenType.OpenVariable);
            string name = tokenizerProxy.Expect(TokenType.Word).Value;
            ArgumentsDeclarationsNode arguments = null;
            if (tokenizerProxy.Accept(TokenType.Colon) != null)
            {
                arguments = ParseArgumentsDefinitions();
            }
            tokenizerProxy.Expect(TokenType.CloseVariable);
            return new VariableDefinitionNode(name, arguments);
        }

        private ConditionsDefinitionsNode ParseConditionsDefinitions()
        {
            List<ConditionDefinitionNode> conditions = new List<ConditionDefinitionNode>();
            if (tokenizerProxy.Accept(TokenType.ConditionsKeyword) != null)
            {
                tokenizerProxy.Expect(TokenType.Colon);
                conditions.Add(ParseSingleConditionDefinition());
                while (tokenizerProxy.Accept(TokenType.Comma) != null)
                {
                    conditions.Add(ParseSingleConditionDefinition());
                }

                tokenizerProxy.Expect(TokenType.Return);
            }

            return new ConditionsDefinitionsNode(conditions);
        }

        private ConditionDefinitionNode ParseSingleConditionDefinition()
        {
            tokenizerProxy.Expect(TokenType.OpenCondition);
            string name = tokenizerProxy.Expect(TokenType.Word).Value;
            ArgumentsDeclarationsNode arguments = null;
            if (tokenizerProxy.Accept(TokenType.Colon) != null)
            {
                arguments = ParseArgumentsDefinitions();
            }
            tokenizerProxy.Expect(TokenType.CloseCondition);
            return new ConditionDefinitionNode(name, arguments);
        }

        private TriggersDefinitionsNode ParseTriggersDefinitions()
        {
            List<TriggerDefinitionNode> triggers = new List<TriggerDefinitionNode>();
            if (tokenizerProxy.Accept(TokenType.TriggersKeyword) != null)
            {
                tokenizerProxy.Expect(TokenType.Colon);
                triggers.Add(ParseSingleTriggerDefinition());
                while (tokenizerProxy.Accept(TokenType.Comma) != null)
                {
                    triggers.Add(ParseSingleTriggerDefinition());
                }

                tokenizerProxy.Expect(TokenType.Return);
            }

            return new TriggersDefinitionsNode(triggers);
        }

        private TriggerDefinitionNode ParseSingleTriggerDefinition()
        {
            tokenizerProxy.Expect(TokenType.OpenTrigger);
            string name = tokenizerProxy.Expect(TokenType.Word).Value;
            ArgumentsDeclarationsNode arguments = null;
            if (tokenizerProxy.Accept(TokenType.Colon)!=null)
            {
                arguments = ParseArgumentsDefinitions();
            }
            tokenizerProxy.Expect(TokenType.CloseTrigger);

            return new TriggerDefinitionNode(name, arguments);
        }

        private ArgumentsDeclarationsNode ParseArgumentsDefinitions()
        {
            List<ArgumentDeclarationNode> arguments = new List<ArgumentDeclarationNode>();
            TypeNode type = ParseType();
            string name = tokenizerProxy.Expect(TokenType.Word).Value;
            arguments.Add(new ArgumentDeclarationNode(type, name));
            while (tokenizerProxy.Accept(TokenType.Comma) != null)
            {
                type = ParseType();
                name = tokenizerProxy.Expect(TokenType.Word).Value;
                arguments.Add(new ArgumentDeclarationNode(type, name));
            }
            return new ArgumentsDeclarationsNode(arguments);
        }
        private TypeNode ParseType()
        {
            bool isMany = tokenizerProxy.Accept(TokenType.Many) != null;
            if (tokenizerProxy.Accept(TokenType.IntType) != null)
            {
                return new IntTypeNode(isMany);
            }

            if (tokenizerProxy.Accept(TokenType.FloatType) != null)
            {
                return new FloatTypeNode(isMany);
            }
            tokenizerProxy.Expect(TokenType.StringType);
            return new StringTypeNode(isMany);
        }
        
        private void ParseBeginKeyword()
        {
            tokenizerProxy.Expect(TokenType.BeginKeyword);
        }
        
        private string ParseLineOfText()
        {
            string lineContent = tokenizerProxy.AcceptLine('\n');
            return lineContent;
        }
    }
}