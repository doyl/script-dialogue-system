﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class DialogueIdParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;

        public DialogueIdParser(ScriptScriptTokenizerProxy tokenizerProxy)
        {
            this.tokenizerProxy = tokenizerProxy;
        }

        public string ParseId()
        {
            DslToken id = tokenizerProxy.Accept(TokenType.Id);
            if (id == null)
            {
                id = tokenizerProxy.ProvideId();
            }

            return id.Value.Substring(1);
        }
    }
}