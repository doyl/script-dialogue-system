﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class ArgumentsParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;

        public ArgumentsParser(ScriptScriptTokenizerProxy tokenizerProxy)
        {
            this.tokenizerProxy = tokenizerProxy;
        } 
        
        public ArgumentsNode Parse()
        {
            List<ArgumentNode> arguments = new List<ArgumentNode>();
            var argumentNode = ParseSingleArgument();
            arguments.Add(argumentNode);
            while (tokenizerProxy.Accept(TokenType.Comma) != null)
            {
                argumentNode = ParseSingleArgument();
                arguments.Add(argumentNode);
            }
            return new ArgumentsNode(arguments);
        }

        private ArgumentNode ParseSingleArgument()
        {
            DslToken token = tokenizerProxy.Accept(TokenType.StringValue);
            if (token != null)
            {
                return new StringArgumentNode(token.Value.Trim('"'));
            }
            token = tokenizerProxy.Accept(TokenType.Number);
            if (token != null)
            {
                return new IntArgumentNode(int.Parse(token.Value));
            }

            token = tokenizerProxy.Expect(TokenType.Number);
            return new IntArgumentNode(int.Parse(token.Value)); //TODO: Should add float value instead of this
        }
    }
}