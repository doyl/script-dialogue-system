﻿using System;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class ScriptScriptException : Exception
    {
        public ScriptScriptException(string message) : base(message)
        {

        }
    }
}