﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Actors;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Lines;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.NodeParsers
{
    public class LineParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly VariableParser variableParser;
        private readonly LineContentParser lineContentParser;

        public LineParser(ScriptScriptTokenizerProxy tokenizerProxy, LineContentParser lineContentParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.lineContentParser = lineContentParser;
        }
        
        public LineNode ParseLine()
        {
            string actorName = tokenizerProxy.Expect(TokenType.Word).Value;
            ActorNode actor = new ActorNode(actorName);
            tokenizerProxy.Expect(TokenType.Colon);
            tokenizerProxy.Expect(TokenType.Return);
            LineContent content = lineContentParser.ParseLineContent( '\n');
            tokenizerProxy.Expect(TokenType.Return);
            return new LineNode(actor, content);
        }
    }
}