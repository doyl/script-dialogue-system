﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.NodeParsers
{
    public class LineContentParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly VariableParser variableParser;

        public LineContentParser(ScriptScriptTokenizerProxy tokenizerProxy, VariableParser variableParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.variableParser = variableParser;
        }
        
        public LineContent ParseLineContent(char until = '^')
        {
            List<string> parts = new List<string>();
            List<DialogueVariableInstance> variables = new List<DialogueVariableInstance>();
            string lineContent = tokenizerProxy.AcceptLine(until, '{');
            parts.Add(lineContent);
            while (tokenizerProxy.Check(TokenType.OpenVariable))
            {
                DialogueVariableInstance variable = variableParser.ParseVariableInstance();
                variables.Add(variable);
                parts.Add(variable.GetVariableId());
                lineContent = tokenizerProxy.AcceptLine(until, '{');
                parts.Add(lineContent);
            }
            return new LineContent(parts, variables);
        }
    }
}