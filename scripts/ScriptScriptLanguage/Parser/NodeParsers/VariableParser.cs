﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Variables;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.NodeParsers
{
    public class VariableParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly ArgumentsParser argumentsParser;

        public VariableParser(ScriptScriptTokenizerProxy tokenizerProxy, ArgumentsParser argumentsParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.argumentsParser = argumentsParser;
        }
        
        public DialogueVariableInstance ParseVariableInstance()
        {
            tokenizerProxy.Expect(TokenType.OpenVariable);
            string name = tokenizerProxy.Expect(TokenType.Word).Value;
            ArgumentsNode arguments = null;
            if (tokenizerProxy.Accept(TokenType.Colon) != null)
            {
                arguments = argumentsParser.Parse();
            }
            tokenizerProxy.Expect(TokenType.CloseVariable);
            List<Argument> realArguments = arguments?.GetArguments();
            return new DialogueVariableInstance(name, realArguments);
        }
    }
}