﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions
{
    public class ConditionDefinitionNode : Node
    {
        public string Name { get; set; }
        public ArgumentsDeclarationsNode Arguments { get; set; }

        public ConditionDefinitionNode(string name, ArgumentsDeclarationsNode arguments)
        {
            this.Name = name;
            this.Arguments = arguments;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}