﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions
{
    public class ConditionsDefinitionsNode : Node
    {
        private readonly List<ConditionDefinitionNode> conditions;

        public ConditionsDefinitionsNode(List<ConditionDefinitionNode> conditions)
        {
            this.conditions = conditions;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            foreach (ConditionDefinitionNode condition in conditions)
            {
                condition.Accept(visitor);
            }
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}