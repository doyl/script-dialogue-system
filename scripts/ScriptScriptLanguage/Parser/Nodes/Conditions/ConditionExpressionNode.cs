﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions
{
    public class ConditionExpressionNode : Node
    {
        public List<List<ConditionUsageNode>> Conditions { get; }

        public ConditionExpressionNode(List<List<ConditionUsageNode>> conditions)
        {
            Conditions = conditions;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}