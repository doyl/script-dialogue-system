﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions
{
    public class ConditionUsageNode : Node
    {
        public string ConditionName { get; set; }
        public ArgumentsNode ArgumentsNode { get; set; }
        public bool IsNegated { get; set; }

        public ConditionUsageNode(string conditionName, ArgumentsNode argumentsNode, bool isNegated)
        {
            this.ConditionName = conditionName;
            this.ArgumentsNode = argumentsNode;
            this.IsNegated = isNegated;
        }

        //public string ConditionName { get; internal set; }

        public override void Accept(IParseTreeVisitor visitor)
        {
            throw new System.NotImplementedException();
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}