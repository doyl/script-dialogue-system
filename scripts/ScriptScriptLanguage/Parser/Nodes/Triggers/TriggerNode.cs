﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Triggers
{
    public class TriggerNode : DialogueElementNode
    {
        public string TriggerName { get; set; }
        public ArgumentsNode Arguments { get; }

        public TriggerNode(string triggerName, ArgumentsNode arguments)
        {
            TriggerName = triggerName;
            Arguments = arguments;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }

        public override DialogueElement GetElement()
        {
            return new DialogueTriggerInvocation(TriggerName, Arguments.GetArguments());
        }
    }
}