﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Triggers
{
    public class TriggersDefinitionsNode : Node
    {
        private readonly List<TriggerDefinitionNode> triggers;

        public TriggersDefinitionsNode(List<TriggerDefinitionNode> triggers)
        {
            this.triggers = triggers;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            foreach (TriggerDefinitionNode trigger in triggers)
            {
                trigger.Accept(visitor);
            }
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}