﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes
{
    public class ScriptScriptDialogueRoot : Node
    {
        readonly BeginningNode beginning;
        readonly DialogueNode dialogue;
        public string Id { get; }

        public ScriptScriptDialogueRoot(BeginningNode beginning, DialogueNode dialogue, string id)
        {
            this.beginning = beginning;
            this.dialogue = dialogue;
            Id = id;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            visitor.Visit(this);
            beginning.Accept(visitor);
            dialogue.Accept(visitor);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}