﻿namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Jumps
{

    public class EndJumpNode : JumpNode
    {
        public EndJumpNode() : base("End")
        { }
    }
}