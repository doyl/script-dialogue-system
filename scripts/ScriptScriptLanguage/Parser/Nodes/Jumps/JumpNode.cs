﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Jumps
{
    public class JumpNode : DialogueElementNode
    {
        public string Tag { get; }
        public JumpNode(string tag)
        {
            this.Tag = tag;
        }

        public override DialogueElement GetElement()
        {
            return new Jump(Tag);
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
        }
    }
}