﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Actors;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Lines
{
    public class LineNode : DialogueElementNode
    {
        readonly ActorNode actor;
        readonly LineContent lineContent;

        public LineNode(ActorNode actor, LineContent lineContent)
        {
            this.actor = actor;
            this.lineContent = lineContent;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
        }

        public override DialogueElement GetElement()
        {
            return new DialogueLine(id, lineContent, actor.Name);
        }
    }
}
