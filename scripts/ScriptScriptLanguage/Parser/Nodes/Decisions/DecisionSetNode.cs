﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Decisions;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Decisions
{
    public class DecisionSetNode : DialogueElementNode
    {
        public List<DecisionNode> Decisions { get; set; }
        public List<Decision> CreatedDecisions { get; set; } = new List<Decision>();

        public DecisionSetNode(List<DecisionNode> decisions)
        {
            this.Decisions = decisions;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            foreach (DecisionNode decision in Decisions)
            {
                decision.Accept(visitor);
            }
            visitor.Visit(this);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }

        public override DialogueElement GetElement()
        {
            return new DecisionSet(id, CreatedDecisions);
        }
    }
}