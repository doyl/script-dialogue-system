﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Lines;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Jumps;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Decisions
{
    public class DecisionNode : Node
    {
        public LineContent Content { get; set; }
        public JumpNode Jump { get; set; }
        public ConditionExpressionNode Conditions { get; set; }
        public DecisionNode(LineContent content, JumpNode jump, ConditionExpressionNode conditions)
        {
            this.Content = content;
            this.Jump = jump;
            this.Conditions = conditions;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            //Conditions.Accept(visitor);
            Conditions?.Accept(visitor);
            visitor.Visit(this);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}