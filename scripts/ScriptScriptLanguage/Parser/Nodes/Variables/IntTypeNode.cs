﻿namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Variables
{
    internal class IntTypeNode : TypeNode
    {
        public IntTypeNode(bool isMany) : base(isMany)
        {
        }
    }
}