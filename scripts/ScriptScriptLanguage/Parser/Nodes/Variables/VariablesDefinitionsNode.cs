﻿using System;
using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Variables
{
    public class VariablesDefinitionsNode : Node
    {
        public List<VariableDefinitionNode> VariablesDefinitions { get; }

        public VariablesDefinitionsNode(List<VariableDefinitionNode> variablesDefinitions)
        {
            VariablesDefinitions = variablesDefinitions;
        }
        public override void Accept(IParseTreeVisitor visitor)
        {
            foreach (var variable in VariablesDefinitions)
            {
                variable.Accept(visitor);
            }
        }

        public override string ContentToString()
        {
            throw new NotImplementedException();
        }
    }

    public class VariableDefinitionNode : Node
    {
        public string VariableName { get; }
        public ArgumentsDeclarationsNode ArgumentsDeclarations { get; }

        public VariableDefinitionNode(string name, ArgumentsDeclarationsNode arguments)
        {
            VariableName = name;
            ArgumentsDeclarations = arguments;
        }
        public override void Accept(IParseTreeVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override string ContentToString()
        {
            throw new NotImplementedException();
        }
    }
}