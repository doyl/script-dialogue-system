﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments
{
    public class ArgumentsNode : Node
    {
        private readonly List<ArgumentNode> arguments;

        public ArgumentsNode(List<ArgumentNode> arguments)
        {
            this.arguments = arguments;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            throw new System.NotImplementedException();
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }

        internal List<Argument> GetArguments()
        {
            return arguments?.ConvertAll(x => x.GetArgument());
        }

        internal static ArgumentsNode Empty()
        {
            return new ArgumentsNode(new List<ArgumentNode>());
        }
    }
}