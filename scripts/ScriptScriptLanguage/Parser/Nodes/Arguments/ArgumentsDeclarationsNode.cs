﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments
{
    public class ArgumentsDeclarationsNode : Node
    {
        private List<ArgumentDeclarationNode> arguments;

        public ArgumentsDeclarationsNode(List<ArgumentDeclarationNode> arguments)
        {
            this.arguments = arguments;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            throw new System.NotImplementedException();
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}