﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments
{
    public class FloatArgumentNode : ArgumentNode
    {
        public float Value { get; }

        public FloatArgumentNode(float value)
        {
            Value = value;
        }
        
        public override void Accept(IParseTreeVisitor visitor)
        {
            throw new System.NotImplementedException();
        }

        public override string ContentToString()
        {
            return Value.ToString();
        }

        public override Argument GetArgument()
        {
            return new FloatArgument(Value);
        }
    }
}
