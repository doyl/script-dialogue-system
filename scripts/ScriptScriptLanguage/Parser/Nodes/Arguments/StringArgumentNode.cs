﻿using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments
{
    public class StringArgumentNode : ArgumentNode
    {
        public string Value { get; }

        public StringArgumentNode(string value)
        {
            Value = value;
        }
        
        public override void Accept(IParseTreeVisitor visitor)
        {
            throw new System.NotImplementedException();
        }

        public override string ContentToString()
        {
            return Value;
        }

        public override Argument GetArgument()
        {
            return new StringArgument(Value);
        }
    }
}
