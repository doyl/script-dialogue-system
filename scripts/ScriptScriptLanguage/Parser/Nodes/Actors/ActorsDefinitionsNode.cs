﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Actors
{
    public class ActorsDefinitionsNode : Node
    {
        public List<ActorNode> Actors { get; set; }

        public ActorsDefinitionsNode(List<ActorNode> actors)
        {
            this.Actors = actors;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            foreach (ActorNode actor in Actors)
            {
                actor.Accept(visitor);
            }
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}