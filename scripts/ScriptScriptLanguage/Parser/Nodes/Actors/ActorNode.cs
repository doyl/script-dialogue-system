﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Actors
{
    public class ActorNode : Node
    {
        public string Name { get; }

        public ActorNode(string name)
        {
            Name = name;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            //visitor.Visit(this); TODO: Make actors actually mean something
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}