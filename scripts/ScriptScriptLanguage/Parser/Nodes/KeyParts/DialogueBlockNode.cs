﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts
{
    public class DialogueBlockNode : Node
    {
        public DialogueElementNode DialogueElement { get; set; }
        public string Tag { get; set; }
        public string Id { get; set; }
        
        public ConditionExpressionNode Conditions { get; set; }

        public DialogueBlockNode(DialogueElementNode dialogueElement, string tag, string id,
            ConditionExpressionNode conditions = null)
        {
            DialogueElement = dialogueElement;
            Tag = tag;
            Id = id;
            Conditions = conditions;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            Conditions?.Accept(visitor);
            DialogueElement.Accept(visitor);
            visitor.Visit(this);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}