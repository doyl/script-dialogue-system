﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts
{
    public class DialogueNode : Node
    {
        private readonly List<DialogueBlockNode> dialogueBlocks;

        public DialogueNode(List<DialogueBlockNode> dialogueBlocks)
        {
            this.dialogueBlocks = dialogueBlocks;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            foreach (DialogueBlockNode block in dialogueBlocks)
            {
                block.Accept(visitor);
            }
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}