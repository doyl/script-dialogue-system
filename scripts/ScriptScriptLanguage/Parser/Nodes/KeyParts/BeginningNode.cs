﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Actors;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Triggers;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Variables;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts
{
    public class BeginningNode : Node
    {
        private TitlesDeifnitionsNode TitlesDefinitions { get; }
        private ActorsDefinitionsNode ActorsDef { get; }
        private VariablesDefinitionsNode VariablesDef { get; }
        private ConditionsDefinitionsNode ConditionsDef { get; }
        private TriggersDefinitionsNode TriggersDef { get; }

        public BeginningNode(TitlesDeifnitionsNode titlesDefinitions, ActorsDefinitionsNode actorsDef, VariablesDefinitionsNode variablesDef, ConditionsDefinitionsNode conditionsDef, TriggersDefinitionsNode triggersDef)
        {
            TitlesDefinitions = titlesDefinitions;
            ActorsDef = actorsDef;
            VariablesDef = variablesDef;
            ConditionsDef = conditionsDef;
            TriggersDef = triggersDef;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            TitlesDefinitions.Accept(visitor);
            ActorsDef.Accept(visitor);
            VariablesDef.Accept(visitor);
            ConditionsDef.Accept(visitor);
            TriggersDef.Accept(visitor);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}