﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts
{
    public abstract class Node
    {
        public abstract void Accept(IParseTreeVisitor visitor);
        public abstract string ContentToString();
    }
}
