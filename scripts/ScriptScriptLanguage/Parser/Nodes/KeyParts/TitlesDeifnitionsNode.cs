﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts
{
    public class TitlesDeifnitionsNode : Node
    {
        public string Title { get; set; }

        public TitlesDeifnitionsNode(string title)
        {
            this.Title = title;
        }

        public override void Accept(IParseTreeVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }
    }
}