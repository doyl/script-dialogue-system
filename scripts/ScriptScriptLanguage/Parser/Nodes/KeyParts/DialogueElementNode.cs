﻿using System;
using Doyl.ScriptDialogueSystem.Scripts.DialogueSystem.core.Dialogues;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Visitors;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts
{
    public abstract class DialogueElementNode : Node
    {
        public Guid id;
        public abstract override void Accept(IParseTreeVisitor visitor);

        public override string ContentToString()
        {
            throw new System.NotImplementedException();
        }

        public abstract DialogueElement GetElement();
    }
}