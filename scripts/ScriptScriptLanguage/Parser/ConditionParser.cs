﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Arguments;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.Conditions;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class ConditionParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly ArgumentsParser argumentsParser;

        public ConditionParser(ScriptScriptTokenizerProxy tokenizerProxy, ArgumentsParser argumentsParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.argumentsParser = argumentsParser;
        }
        
        public ConditionExpressionNode ParseConditionsExpression()
        {
            List<List<ConditionUsageNode>> conditions = new List<List<ConditionUsageNode>>();
            List<ConditionUsageNode> currentConditionsConjunction = new List<ConditionUsageNode>();
            conditions.Add(currentConditionsConjunction);
            currentConditionsConjunction.Add(ParseCondition());
            DslToken booleanOperator;
            while ((booleanOperator = tokenizerProxy.Accept(TokenType.BooleanOperator)) != null)
            {
                ConditionUsageNode condition = ParseCondition();
                if (booleanOperator.Value == "|")
                {
                    currentConditionsConjunction = new List<ConditionUsageNode>();
                    conditions.Add(currentConditionsConjunction);
                    currentConditionsConjunction.Add(condition);
                }
                else if (booleanOperator.Value == "&")
                {
                    currentConditionsConjunction.Add(condition);
                }
            }
            return new ConditionExpressionNode(conditions);
        }
        
        private ConditionUsageNode ParseCondition()
        {
            bool isNegated = tokenizerProxy.Accept(TokenType.NegateOperator) != null;
            tokenizerProxy.Expect(TokenType.OpenCondition);
            string condName = tokenizerProxy.Expect(TokenType.Word).Value;
            ArgumentsNode argumentsNode = ArgumentsNode.Empty();
            if (tokenizerProxy.Accept(TokenType.Colon) != null)
            {
                argumentsNode = argumentsParser.Parse();
            }
            tokenizerProxy.Expect(TokenType.CloseCondition);
            return new ConditionUsageNode(condName, argumentsNode, isNegated);
        }
    }
}