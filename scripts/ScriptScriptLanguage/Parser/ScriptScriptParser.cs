﻿using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class ScriptScriptParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly BeginningParser beginningParser;
        private readonly DialogueParser dialogueParser;
        private readonly DialogueIdParser dialogueIdParser;

        public ScriptScriptParser(ScriptScriptTokenizerProxy tokenizerProxy, BeginningParser beginningParser, DialogueParser dialogueParser, DialogueIdParser dialogueIdParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.beginningParser = beginningParser;
            this.dialogueParser = dialogueParser;
            this.dialogueIdParser = dialogueIdParser;
        }

        public ScriptScriptDialogueRoot ParseDialogueScript(out string resultScript)
        {
            tokenizerProxy.NextToken();
            string id = dialogueIdParser.ParseId();
            BeginningNode beginning = beginningParser.Parse();
            DialogueNode dialogue = dialogueParser.ParseDialogue();
            resultScript = tokenizerProxy.GetResultText();
            return new ScriptScriptDialogueRoot(beginning, dialogue, id);
        }
    }
}