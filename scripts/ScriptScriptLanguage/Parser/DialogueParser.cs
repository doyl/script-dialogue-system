﻿using System.Collections.Generic;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser.Nodes.KeyParts;
using Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Tokenizer;

namespace Doyl.ScriptDialogueSystem.Scripts.ScriptScriptLanguage.Parser
{
    public class DialogueParser
    {
        private readonly ScriptScriptTokenizerProxy tokenizerProxy;
        private readonly DialogueBlockParser dialogueBlockParser;

        public DialogueParser(ScriptScriptTokenizerProxy tokenizerProxy, DialogueBlockParser dialogueBlockParser)
        {
            this.tokenizerProxy = tokenizerProxy;
            this.dialogueBlockParser = dialogueBlockParser;
        }

        public DialogueNode ParseDialogue()
        {
            List<DialogueBlockNode> dialogueBlocks = new List<DialogueBlockNode>();
            dialogueBlocks.Add(dialogueBlockParser.ParseDialogueBlock());
            while (tokenizerProxy.Check(TokenType.Tag)
                   || tokenizerProxy.Check(TokenType.Word)
                   || tokenizerProxy.Check(TokenType.Number)
                   || tokenizerProxy.Check(TokenType.Jump)
                   || tokenizerProxy.Check(TokenType.OpenTrigger)
                   || tokenizerProxy.Check(TokenType.OpenCondition)
                   || tokenizerProxy.Check(TokenType.NegateOperator))
            {
                dialogueBlocks.Add(dialogueBlockParser.ParseDialogueBlock());
            }
            tokenizerProxy.Expect(TokenType.EndKeyword);
            return new DialogueNode(dialogueBlocks);
        }
    }
}